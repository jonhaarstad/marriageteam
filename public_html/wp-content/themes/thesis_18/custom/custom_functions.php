<?php

// Using hooks is absolutely the smartest, most bulletproof way to implement things like plugins,
// custom design elements, and ads. You can add your hook calls below, and they should take the 
// following form:
// add_action('thesis_hook_name', 'function_name');
// The function you name above will run at the location of the specified hook. The example
// hook below demonstrates how you can insert Thesis' default recent posts widget above
// the content in Sidebar 1:
// add_action('thesis_hook_before_sidebar_1', 'thesis_widget_recent_posts');

// Delete this line, including the dashes to the left, and add your hooks in its place.

/**
 * function custom_bookmark_links() - outputs an HTML list of bookmarking links
 * NOTE: This only works when called from inside the WordPress loop!
 * SECOND NOTE: This is really just a sample function to show you how to use custom functions!
 *
 * @since 1.0
 * @global object $post
*/





function custom_bookmark_links() {
	global $post;
	?>
	<ul class="bookmark_links">
		<li><a rel="nofollow" href="http://delicious.com/save?url=<?php urlencode(the_permalink()); ?>&amp;title=<?php urlencode(the_title()); ?>" onclick="window.open('http://delicious.com/save?v=5&amp;noui&amp;jump=close&amp;url=<?php urlencode(the_permalink()); ?>&amp;title=<?php urlencode(the_title()); ?>', 'delicious', 'toolbar=no,width=550,height=550'); return false;" title="Bookmark this post on del.icio.us">Bookmark this article on Delicious</a></li>
	</ul>
	<?php
}

if ( function_exists('register_sidebar') )
	register_sidebar(array(
	'name'=>'Media Box',
	'before_widget' => '<li class="widget %2$s">',
	'after_widget' => '</li>',
	'before_title' => '<h3>',
	'after_title' => '</h3>'));

function mediabox_widget() {
	echo '<ul class="sidebar_list">';
	if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('Media Box') ) :
	echo '</ul>';
	endif;
}
add_action('thesis_hook_multimedia_box', 'mediabox_widget');

function show_mmbox() { 
    thesis_multimedia_box(); 
} 
add_action('thesis_hook_before_content','show_mmbox'); 

if (file_exists(THESIS_CUSTOM . '/places_template_functions.php')){
	include(THESIS_CUSTOM . '/places_template_functions.php');
}

/* REMOVE HOME PAGE FOOTER */
function no_footer() {
if (is_page('home'))
	return false;
else
	return true;
}
add_filter('thesis_show_footer', 'no_footer');

/* BRAND NEW FOOTER
function create_internal_footer() { ?>
	<div id="footer_new">
		<p>&copy; 2011 MarriageTeam. All rights reserved. <a href="http://www.ourblog.com/privacy-policy/">Privacy Policy</a></p>
		<p>No content on this site may be reused in any fashion without written permission from MarriageTeam.</p>
		</div>
	<?php	
	}
add_action('thesis_hook_after_footer', 'create_internal_footer');

/* CUSTOMIZE THE FOOTER 
if(is_home()) {
	remove_action('thesis_hook_footer');
}
remove_action('thesis_hook_footer', 'thesis_attribution');

function add_custom_footer () {
	?>
	<p>&copy; 2011 MarriageTeam. All rights reserved. <a href="http://www.ourblog.com/privacy-policy/">Privacy Policy</a></p>
	<p>No content on this site may be reused in any fashion without written permission from MarriageTeam.</p>
	<?php
}
add_action('thesis_hook_footer', 'add_custom_footer');



/* HOME PAGE CUSTOM TEMPLATE */

function home_pagecustom() {
/* check to see if homepage. has to happen inside the function */
/* if (is_home() || is_front_page())  { */
	if (is_page(4293)) {
	?>
	...Your custom layout goes here...
	<?php }
}

/* Now we tell Thesis to use the home page custom template */
remove_action('thesis_hook_custom_template', 'thesis_custom_template_sample');
add_action('thesis_hook_custom_template', 'home_pagecustom');


/* CUSTOMIZE THE TEMPLATES BEING USED */

/* INTERNAL TEMPLATE 1 */

function custom_internal_one() {
?>
	<div id="quicklinks">
		<p><strong>QuickLinks</strong><br />
		Category Name Here</p>
		<ul>
			<li><a href="#">Subnav 1</a></li>
			<li><a href="#">Subnav 2</a></li>
			<li><a href="#">Subnav 3</a></li>
			<li><a href="#">Subnav 4</a></li>
			<li><a href="#">Subnav 5</a></li>
		</ul>
	</div>
<?php
}

if (isset($internal_template)) {
	add_action('thesis_hook_before_post', 'custom_internal_one');
}
else {

}

/* INTERAL TEMPLATE NUMBER 1 

function custom_internal() {
	if (is_page()) {
	?>
		<div id="quicklinks">
			<p><strong>QuickLinks</strong><br />
			Category Name Here</p>
			<ul>
				<li><a href="#">Subnav 1</a></li>
				<li><a href="#">Subnav 2</a></li>
				<li><a href="#">Subnav 3</a></li>
				<li><a href="#">Subnav 4</a></li>
				<li><a href="#">Subnav 5</a></li>
			</ul>
		</div>
	<?php
}
elseif (is_home()) {
	?>
	<p>something here on the home page</p>
	<?php
}
add_action('thesis_hook_before_post', 'template_one'); */

/* Custom 404 Hooks */

function custom_thesis_404_title() {
	?>
	Oops - you found a page <br />that doesn't exist.
	<?
}
remove_action('thesis_hook_404_title', 'thesis_404_title');
add_action('thesis_hook_404_title', 'custom_thesis_404_title');

function custom_thesis_404_content() {
	?>
	<p>Either a page has moved or a link has been broken. Click your back button to return <br />to the previous page.</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<p>&nbsp;</p>
	<?
}
remove_action('thesis_hook_404_content', 'thesis_404_content');
add_action('thesis_hook_404_content', 'custom_thesis_404_content');