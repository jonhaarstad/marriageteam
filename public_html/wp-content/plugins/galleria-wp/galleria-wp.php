<?php
/*
Plugin Name: Galleria WP
Plugin URI: http://y2itec.com/samples/wordpress/galleria-wp/
Description: A simple image gallery for WordPress2.5 using galleria 1.0 image gallery. 
 Galleria  <a href="http://devkick.com/lab/galleria/> DEVKICK </a>
 jQuery <a href="http://jquery.com/> jQuery </a>
 jCarousel <a href="http://sorgalla.com/jcarousel/>jCarousel</a>
Author: Y2 (Yokoyama  Yasuaki)
Author URI: http://y2itec.com/
Version: 1.2.5
*/
/*	Copyright 2008	Y2 (Yokoyama Yasuaki)  

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/
//==============================================================================//
//    function galleria_shortcode(): WP short code application for              //
//                                   Galleria image gallery                     //
//                                                                              //
//    Usage: [galleria <options>]                                               //
//           options: orderby             sorting order                         //
//                    id                  post ID                               //
//                    size                WP thumbnail size                     //
//                                           (defualt: 'thumbnail')             //
//                    gallery="<number>"  Use NextGen Gallery DB[ gallery=xxx ] //
//                    tags="<tags>"       Use NextGen Gallery DB                //
//                                          [ tags=abc, cde, ... ]              //
//                    album="<number>"    Use NextGen Gallery DB  [ album=xxx ] //
//                    columns             thumbnail columns                     //
//                                        ( thumbnail="top/bottom-scroll" )     //
//                    rows                thumbnail rows                        //
//                                        ( thumbnail="left/right-scroll" )     //
//                    steps               thumbnail forward/backward step size  //
//                    exif="<0/1>"        show EXIF meta data (default: "0")    //
//                    gid="<string>"      use for grouping                      //
//                    playbutton="1"      add automatic slide playback button   //
//                    interval="<sec>"    display intervals ( second )          //
//                    showimage="<yes/no>" default : "yes"                      //
//                                                   "no" : do not show image   //
//                    shrink="<0/1>" (default is "1" : shrink to fit the stage) //
//                    dblclick="<source/post>" behavior of double click         //
//                                                     operation on thumbnail   //
//                                 default : "source" open original image in    //
//                                                    new window                //
//                                           "post"   open attachment post page //
//                                                    in new window             //
//                                                                              //
//                                           "current" : subordinate option     //
//                    If you concatenate "current" keyword to dblclick option,  //
//                    contents will be opend in current window.                 //
//                         Examples : "post-current" --> open attachment post   //
//                                                       page in current window //
//                                                                              //
//                                    "current" --> same as "source-current"    //
//                    cropthumb="<yes/no>" default : "no" :                     //
//                    thumbnail="<keyword>"   thumbnail options                 //
//                              'top'                                           //
//                              'bottom'                                        //
//                              'top-scroll' (default)                          //
//                              'bottom-scroll'                                 //
//                              'none'   hide thumbnails                        //
//                              'left-scroll' (reserved)                        //
//                              'right-scroll' (reserved)                       //
//                    navigation="<keyword>" location of the navigation link    //
//                              'top'                                           //
//                              'bottom' (default)                              //
//                              'none'  hide navigation items                   //
//                    index="<keyword">   location of the image index number    //
//                              'top'                                           //
//                              'bottom'    (default)                           //
//                              'caption1'  inject to the first caption line    //
//                              'caption2'  inject to the second caption line   //
//                              'none'   hide index                             //
//                    thumb_w="<width>"  thumbnail image width  [px]            //
//                    thumb_h="<height>" thumbnail image height [px]            //
//                    cssclass="<string>" CSS class selector                    //
//                                         (Ex. cssclass="galleria_black" )     //
//                    background_style="<styles>"  background styles            //
//                    caption1_style="<styles>"    caption #1 styles            //
//                    caption2_style="<styles>"    caption #2 styles            //
//                                                                              //
//                    orderby, id, size => same as builtin [gallery] short code //
//                                                                              //
//    Licenses : Galleria WP is licensed under the GPL licenses.                //
//                                                                              //
//               Galleria is licensed under the GPL licenses.                   //
//               [ http://devkick.com/lab/galleria/ ]                           //
//                                                                              //
//               jCarousel is licensed under both MIT and GPL licenses.         //
//               [ http://sorgalla.com/jcarousel/ ]                             //
//                                                                              //
//               jQuery is licensed under both MIT and GPL licenses.            //
//               [ http://jquery.com/ ]                                         //
//                                                                              //
//               NextGen Gallery                                                //
//            http://alexrabe.boelinger.com/wordpress-plugins/nextgen-gallery/  //
//    V1.0.0 : Test release version                                             //
//    V1.0.3 : First release version via WordPress Plugin Directory             //
//    V1.1.0  : Multiple stage version                                          //
//    V1.1.2 : Thumbnail background style                                       //
//    V1.2.0 : Thumbnail scroll option (jCarousel)                              //
//             Special thanks to Mr. Papa [ http://cruisetalk.org/ ]            //
//                                                                              //
//    1.2.5 Build #708                      Last Updated: Jun 23rd, 2008  by Y2 //
//==============================================================================//
if(preg_match('#' . basename(__FILE__) . '#', $_SERVER['PHP_SELF'])) { 
	die('Illegal Entry'); 
}

//============================== insert HTML header tag ========================//
add_action( 'wp_head', 'galleria_wp_headers', 10 );
function galleria_wp_headers() {

	$galleria_wp_plugin_path = 
		get_option('siteurl')."/wp-content/plugins/galleria-wp";
		
	$galleria_wp_style_path = "";
	
	$galleria_wp_style_path .= 
		"<link rel=\"stylesheet\" type=\"text/css\" " . 
		"href=\"$galleria_wp_plugin_path/css/galleria_wp.css\" media=\"screen\" />\n";

	   $galleria_wp_style_path .= 
	   "<link rel=\"stylesheet\" type=\"text/css\" " . 
	   "href=\"$galleria_wp_plugin_path/skins/metal/skin.css\" media=\"screen\" />\n";
	   
	$galleria_wp_script_path = "";
	
	$galleria_wp_script_path .= 
		"<script type='text/javascript' ". 
		"src='$galleria_wp_plugin_path/js/jquery-1.2.6.min.js'></script>\n";
	
	$galleria_wp_script_path .= 
		"<script type='text/javascript' ".
		"src='$galleria_wp_plugin_path/js/jquery.galleria.js'></script>\n";

	$galleria_wp_script_path .= 
		"<script type='text/javascript' ".
		"src='$galleria_wp_plugin_path/js/jquery.jcarousel.js'></script>\n";

	echo "<!--	Galleria WP	 [ BEGIN ] --> \n";
	echo $galleria_wp_style_path;
	echo $galleria_wp_script_path;
	echo "<!--	Galleria WP	 [ END ] --> \n";
}

//==============================  Javascript Loader  ==========================//
//add_action( 'get_header', 'galleria_wp_load_js', 30);
function galleria_wp_load_js() {

	$galleria_wp_plugin_path = 
		get_option('siteurl')."/wp-content/plugins/galleria-wp";

	wp_enqueue_script('jquery'); // path : '/wp-includes/js/jquery/jquery.js'

	wp_enqueue_script('jcarousel', 
					  $galleria_wp_plugin_path .'/js/jquery.jcarousel.js', 
					  array('jquery')); 

	wp_enqueue_script('galleria', 
					  $galleria_wp_plugin_path .'/js/jquery.galleria.js', 
					  array('jquery'));
}

//========================= register galleria admin menu ======================//
include_once (dirname (__FILE__)."/galleria-wp-admin.php");

add_action('admin_menu', 'galleria_wp_add_option_page');
function galleria_wp_add_option_page() {

	// Add a new submenu under Options:
	add_options_page('Galleria WP', 'Galleria WP Options', 8,
	 				 __FILE__, 'galleria_wp_admin_options_handler');
}

//========================= count galleria shortcode tags ======================//
add_filter( 'the_content', 'galleria_filter_hook_gallery_counter', 1 );
function galleria_filter_hook_gallery_counter( $content ) {

	global $gNumOfGalleries;

	$keyword = "[galleria";
	$gNumOfGalleries = substr_count( $content, $keyword );

	return $content;
}

//============================== load language resource ========================//
define('GALLERIA_WP_DIR_NAME', dirname(plugin_basename(__FILE__)));
add_action( 'init', 'galleria_wp_load_textdomain' );
function galleria_wp_load_textdomain() {
	load_plugin_textdomain( 'galleria-wp','wp-content/plugins/'.GALLERIA_WP_DIR_NAME.'/lang');
}

//===================================== globals ================================//
$gSeqNum = 0;
$gNumOfGalleries = 0;
$gGalleryClass[0] = '';
$gGalleriaJavaScriptText;
$gjCarouselJavaScriptText;
$gCarouselAttrColumns[0] = 0;
$gCarouselAttrSteps[0] = 0;
$gCarouselThumbWidth[0] = 0;
$gCarouselThumbHeight[0] = 0;
$gCarouselIsVertical[0] = false;
$gCarouselAutoHideButton[0] = 1;

//==============================================================================//
//   main entry :  galleria_shortcode()                                         //
//==============================================================================//
add_shortcode( 'galleria', 'galleria_shortcode' );
function galleria_shortcode( $attr ) {

	global $post;

	global $gSeqNum;
	global $gNumOfGalleries;
	global $gGalleryClass;
	global $gGalleriaJavaScriptText;
	global $gjCarouselJavaScriptText;
	global $gCarouselAttrColumns;
	global $gCarouselAttrSteps;
	global $gCarouselThumbWidth;
	global $gCarouselThumbHeight;
	global $gCarouselIsVertical;
	global $gCarouselAutoHideButton;

	if ( isset( $attr['orderby'] ) ) {
		$attr['orderby'] = sanitize_sql_orderby( $attr['orderby'] );
		if ( !$attr['orderby'] )
			unset( $attr['orderby'] );
	}

	// Read in existing option value from database
	$default_options = (array) (get_option( 'galleria_wp_options' ) ? 
								get_option( 'galleria_wp_options' ) : null );

	// extract EXIF tag settings
	$exif_tags = array(
		'exif_tag_date'     => $default_options['exif_tag_date'],
		'exif_tag_aperture' => $default_options['exif_tag_aperture'],
		'exif_tag_exposure' => $default_options['exif_tag_exposure'],
		'exif_tag_model'    => $default_options['exif_tag_model']
	);

	// get command line options
	extract( shortcode_atts( array(
		'orderby'    => ($default_options['orderby'] ? $default_options['orderby'] : 'menu_order ASC, ID ASC'),
		'id'         => $post->ID,
		'size'       => ($default_options['size'] ? $default_options['size'] : 'thumbnail'),
		'columns'    => ($default_options['columns'] ? $default_options['columns'] : 0),
		'rows'       => ($default_options['columns'] ? $default_options['columns'] : 0), 
		'gallery'    => '',
		'tags'       => '',
		'album'      => '',
		'gid'        => '',
		'exif'       => (($default_options['exif'] == "yes") ? 1 : 0),
		'playbutton' => (($default_options['playbutton'] == "yes") ? 1 : 0),
		'interval'   => ($default_options['interval'] ? $default_options['interval'] : 8.0 ),
		'shrink'     => (($default_options['shrink'] == "no") ? 0 : 1),
		'dblclick'   => ($default_options['dblclick'] ? $default_options['dblclick'] : 'source'),
		'showimage'  => ($default_options['showimage'] ? $default_options['showimage'] : 'yes'),
		'navigation' => ($default_options['navigation'] ? $default_options['navigation'] :'bottom'),
		'index'      => ($default_options['index'] ? $default_options['index'] : 'bottom'),
		'thumbnail'  => ($default_options['thumbnail'] ? $default_options['thumbnail'] : 'top-scroll'),
		'thumb_w'    => ($default_options['thumb_w'] ? $default_options['thumb_w'] : 0),
		'thumb_h'    => ($default_options['thumb_h'] ? $default_options['thumb_h'] : 0),
		'steps'      => ($default_options['steps'] ? $default_options['steps'] : 0),
		'cropthumb'  => (($default_options['cropthumb'] == "yes") ? 'yes' : 'no'),
		'cssclass'   => ($default_options['cssclass'] ? $default_options['cssclass'] : 'galleria_std'),
		'background_style' => '',
		'thumbbox_style'   => '',
		'stage_style'      => '',
		'caption1_style'   => '',
		'caption2_style'   => '',
		'archives'   => 'silent',
		'auto_hide'  =>  (($default_options['auto_hide'] == "no") ? 0 : 1)
	), $attr ) );

	$id = intval($id);
	$columns = intval($columns);
	$rows = intval($rows);
	$steps = intval($steps) ? intval($steps) : $columns ;
	$index = trim($index);
	$classString = trim($cssclass);
	$interval = (double) $interval;
	$thumb_w = intval($thumb_w);
	$thumb_h = intval($thumb_h);
	$archives = trim($archives);

	$given_background_css_properties = trim($background_style);
	$given_caption1_css_properties = trim($caption1_style);
	$given_caption2_css_properties = trim($caption2_style);
	$given_stage_css_properties  = trim($stage_style);
	$given_thumb_css_properties  = trim($thumbbox_style);

	$reset_margin_padding = '';
	$inline_css_properties = '';
	$li_css_properties = '';

	$carousel_class = '';
	if ( strstr(strtolower(trim($thumbnail)) , 'scroll' ) && 
		!strstr(strtolower(trim($thumbnail)) , 'none' ) ) {
		$carousel_class = 'jcarousel-skin-metal';
	}
	
	$hideThumbnail = 0;
	if ( strstr(strtolower(trim($thumbnail)) , 'none' )  ) {
		$hideThumbnail = 1;
	}
	
	$showNavigation = 1;
	if ( strstr(strtolower(trim($navigation)) , 'none' )  ) {
		$showNavigation = 0;
	}

	$doShowImage = 1;
	if ( strstr(strtolower(trim($showimage)) , 'no' )  || 
	     strstr(strtolower(trim($showimage)) , '0' ) ) {
		$doShowImage = 0;
		$showNavigation = 0;
	}

	$doCropThumbnail = 0;
	if ( strstr(strtolower(trim($cropthumb)) , 'yes' )  || 
	     strstr(strtolower(trim($cropthumb)) , '1' ) ) {
		$doCropThumbnail = 1;
	}
	
	$auto_hide_button = 0;
	if ( $auto_hide ) {
		$auto_hide_button = 1;
	}

	$thumbnail_position ='top';
	$is_vertical = false;

	if ( strstr(strtolower(trim($dblclick)) , 'source' ) ) {
		$thumbnail_caption = __('Double Click to Show Full Size Image','galleria-wp');
	} else  if ( strstr(strtolower(trim($dblclick)) , 'post' ) ) {
		$thumbnail_caption = __('Double Click to Show Attachment Page','galleria-wp');
	} else {
		$thumbnail_caption = '';
	}

	$thumbnail_mode = strtolower(trim($thumbnail));
	if ( strstr( $thumbnail_mode, 'top' ) ) {
		$thumbnail_position ='top';
		$is_vertical = false;
		$stage_css_properties  
			= ($default_options['stage_top_css'] ? $default_options['stage_top_css'] : '' );
		$thumb_css_properties  
			= ($default_options['thumb_top_css'] ? $default_options['thumb_top_css'] : '' );
	} else if ( strstr( $thumbnail_mode, 'bottom' ) ) {
		$thumbnail_position = 'bottom';
		$is_vertical = false;
		$stage_css_properties  
			= ($default_options['stage_bottom_css'] ? $default_options['stage_bottom_css'] : '');
		$thumb_css_properties  
			= ($default_options['thumb_bottom_css'] ? $default_options['thumb_bottom_css'] : '');
	} else if ( strstr( $thumbnail_mode, 'left' ) ) {
		$thumbnail_position = 'left';
		$is_vertical = true;
		$stage_css_properties  
			= ($default_options['stage_left_css'] ? $default_options['stage_left_css'] : 'width: 80%;');
		$thumb_css_properties  
			= ($default_options['thumb_left_css'] ? $default_options['thumb_left_css'] : 'width: 19%;');
	} else if ( strstr( $thumbnail_mode, 'right' ) ) {
		$thumbnail_position = 'right';
		$is_vertical = true;
		$stage_css_properties  
			= ($default_options['stage_right_css'] ? $default_options['stage_right_css'] : 'width: 80%;');
		$thumb_css_properties  
			= ($default_options['thumb_right_css'] ? $default_options['thumb_right_css'] : 'width: 19%;');
	}
	if ( $is_vertical ) $thumbnail_caption = '';

	if ( $columns == 0 && $rows > 0 )  $columns = $rows;

	if ( $given_stage_css_properties ) {
		$stage_css_properties = $given_stage_css_properties;
	}
	if ( $given_thumb_css_properties ) {
		$thumb_css_properties = $given_thumb_css_properties;
	}
	
	$background_css_prpperties = '';
	if ( $given_background_css_properties ) {
		$background_css_prpperties = "style=\"".$given_background_css_properties."\"";
	}
	$caption1_css_properties = '';
	if ( $given_caption1_css_properties ) {
		$caption1_css_properties = $given_caption1_css_properties;
	}
	$caption2_css_properties = '';
	if ( $given_caption2_css_properties ) {
		$caption2_css_properties = $given_caption2_css_properties;
	}

	$gSeqNum++;  // galleria short code tag counter
	$galleryID = "gallery".$gSeqNum; // identifies each gallery
	$output_buffer ="";

	// restore attributes (jCarousel)
	$gGalleryClass[$gSeqNum]        = $galleryID;
	$gCarouselAttrColumns[$gSeqNum] = $columns;
	$gCarouselAttrSteps[$gSeqNum]   = $steps;
	$gCarouselThumbWidth[$gSeqNum]  = $thumb_w;
	$gCarouselThumbHeight[$gSeqNum] = $thumb_h;
	$gCarouselIsVertical[$gSeqNum]  = $is_vertical;
	$gCarouselAutoHideButton[$gSeqNum] = $auto_hide_button;

	// is this single page or post ?
	if ( !is_page() && !is_single() ) {
		if ( $archives == 'shortcode' ) {
			$output_buffer .= "[galleria ";
			foreach( $attr as $key => $value ) {
				$output_buffer .= $key."=".$value." ";
			}
			$output_buffer .= "]\n";
			return $output_buffer;
		}
		return '';// nothing to do
	}

	$attachments = 
	get_children("post_parent=$id&post_type=attachment&post_mime_type=image&orderby={$orderby}");

	if ( empty($attachments) ) {
		return '';  // do nothing
	}
	if ( is_feed() ) {  // output standard image links
		$output_buffer = "\n";
		foreach ( $attachments as $id => $attachment )
			$output_buffer .= wp_get_attachment_link( $id, $size, true ) . "\n";
		return $output_buffer;
	}

	// outer wrapper tag
	if ( $gSeqNum == 1 ) {
		$output_buffer .= apply_filters( 'gallery_style',"	<div id='galleria_wp'>\n" );
	}
	// css class wrapper
	$output_buffer .= apply_filters( 'gallery_style',"
		<div class='$classString'>
			<div class='$galleryID' $background_css_prpperties>\n");

	// insert navigation tags here
	if ( $navigation == 'top' && $showNavigation ) {
		$output_buffer .= galleria_generate_navigation_tags( $galleryID );
	}

	// insert <div> and  <ul> tags 
	$output_buffer .= galleria_generate_pre_list_tags( $galleryID, 
													   $thumbnail_position,
													   $thumb_css_properties,
													   $carousel_class,
													   $stage_css_properties );

	$li_width_css  = ($thumb_w > 0) ? "width: $thumb_w"."px; "  : "";
	$li_height_css = ($thumb_h > 0) ? "height: $thumb_h"."px; "  : "";

	if ( $reset_margin_padding  || $li_width_css || $li_height_css ) {
		$li_css_properties 
			= " style='".$li_width_css.$li_height_css.$reset_margin_padding."'";
	} else {
		$li_css_properties = ''; 
	}

	// insert <li> tags 
	$counter = 0;
	foreach ( $attachments as $id => $attachment ) {

		$link = galleria_wp_get_attachment_link( $id, $size, $gid, $exif, $exif_tags );

		if ( !$link )	continue; 

		if ( $counter == 0 ) {
			// activate the first image
			$output_buffer .= apply_filters( 'gallery_style',"
			<li class='active galleria_wp' $li_css_properties>
				$link
			</li>\n");

		} else {

			$output_buffer .= apply_filters( 'gallery_style',"
			<li class='galleria_wp' $li_css_properties>
				$link
			</li>\n");

		}
		$counter++;
	} // end foreach

	// insert  caption tags
	$output_buffer .= galleria_generate_after_list_tags( $galleryID, 
														 $thumbnail_position,
														 $carousel_class,
														 $stage_css_properties );

	// insert navigation tags here
	if ( $navigation == 'bottom' && $showNavigation ) {
		$output_buffer .= galleria_generate_navigation_tags( $galleryID );
	}

	$output_buffer .= apply_filters( 'gallery_style',
"
		</div>
</div>	 <!-- end of [ div class='$classString' ] -->\n");

	// prepare Javascript code
	if ( $gSeqNum == 1 ) { ;
		$gGalleriaJavaScriptText = "";
		$gjCarouselJavaScriptText = "";
		$gJavaScriptTagText = "";
	}
	
	
	$gGalleriaJavaScriptText .= prepareGalleriaScript( $classString,
													   $galleryID,
													   $doShowImage,
													   $is_vertical,
													   $showNavigation,
													   $carousel_class,
													   $doCropThumbnail,
													   $hideThumbnail,
													   $columns,
													   $thumbnail_caption,
													   $playbutton,
													   $interval,
													   $shrink,
													   $index,
													   $dblclick,
													   $caption1_css_properties,
													   $caption2_css_properties );

	$gGalleriaJavaScriptText .= "\n";

	if ( $gSeqNum == $gNumOfGalleries ) {

		$output_buffer .= apply_filters( 'gallery_style', 
"	</div> <!-- end of [ <div id='galleria_wp' class='galeria_wp'> ] --> \n" );
		
		if ( $carousel_class ) {
			$gjCarouselJavaScriptText .= preparejCarouselCallback( $gNumOfGalleries ) ;
			$gjCarouselJavaScriptText .= preparejCarouselScript( $gNumOfGalleries ) ;
			$gjCarouselJavaScriptText .= "\n";
		} else {
			$gjCarouselJavaScriptText .= apply_filters( 'gallery_style',"
function jcarousel_scroll_to_active_item( target_class ) {
	// dummy hook 
	return;
}\n" );
		}
		
		$output_buffer .= apply_filters( 'gallery_style', "<script type='text/javascript'>\n" );
		$output_buffer .= apply_filters( 'gallery_style', $gjCarouselJavaScriptText );
		$output_buffer .= apply_filters( 'gallery_style', $gGalleriaJavaScriptText );
		$output_buffer .= apply_filters( 'gallery_style', "</script>\n" );
	}
	return $output_buffer;
}

//==============================================================================//
//   galleria_generate_navigation_tags( $galleryID )                            //
//==============================================================================//
function galleria_generate_navigation_tags( $galleryID ) {

	$output = "
			<p style='clear: both;'></p>
			<p class='galleria_nav'>
				<a href='#' onclick=\"jQuery.galleria.prev( 'stage_$galleryID','$galleryID' ); return false;\">
					&laquo; ".__('Previous','galleria-wp')."</a> &nbsp;&nbsp;&nbsp; 
				<a href='#' onclick=\"jQuery.galleria.next( 'stage_$galleryID','$galleryID' ); return false;\">
					".__('Next','galleria-wp')." &raquo;</a>
			</p>
			<button class='play'>".__('Play','galleria-wp')."</button>
			<p style='clear: both;'></p>";

	return apply_filters( 'gallery_style', $output );
}

//==============================================================================//
//   galleria_generate_pre_list_tags()                                          //
//==============================================================================//
function galleria_generate_pre_list_tags( $galleryID, 
										  $thumbnail_position,
										  $thumb_css_properties,
										  $carousel_class,
										  $stage_css_properties ) {

	$output = '';
	$ul_css_properties = '';
	$user_css_properties = '';
	
	switch ( $thumbnail_position ) {
		
		case 'top' :

			if ( $thumb_css_properties ) {
				$user_css_properties = 
					"style=\"" .$thumb_css_properties."\""; 
			}
			
			if ( $carousel_class ) {

				$output .= "
			<div class='galleria_thumb $galleryID' $user_css_properties>
				<div class='$carousel_class $galleryID'>
					<div class='jcarousel-container $galleryID'>
						<div class='jcarousel-clip $galleryID'>
							<ul class='galleria_wp $carousel_class $galleryID' $ul_css_properties>\n";
			} else {
			
				$output .= "
			<div class='galleria_thumb $galleryID' $user_css_properties>
				<ul class='galleria_wp $galleryID'  $ul_css_properties>\n";

			}

			break;

		case 'bottom' :
		
			if ( $thumb_css_properties ) {
				$user_css_properties = 
					"style=\"" .$thumb_css_properties."\""; 
			}

			if ( $stage_css_properties ) {
				$stage_css_properties = 
					"style=\"".$stage_css_properties."\"";
			}

			if ( $carousel_class ) {
			
				$output .= "
			<div id='stage_$galleryID' $stage_css_properties>
			<!-- image and caption will be inserted here. -->
			</div>
			<div class='galleria_thumb $galleryID' $user_css_properties>
				<div class='$carousel_class $galleryID'>
					<div class='jcarousel-container $galleryID'>
						<div class='jcarousel-clip $galleryID'>
							<ul class='galleria_wp $carousel_class $galleryID' $ul_css_properties>\n";

			} else {
			
				$output .= "
			<div id='stage_$galleryID' $stage_css_properties>
				<!-- image and caption will be inserted here. -->
			</div>
			<div class='galleria_thumb $galleryID' $ul_css_properties>
				<ul class='galleria_wp $galleryID' >\n";

			}

			break;
		
		case 'left' :

			$user_css_properties = 
					"style=\"float: left; " .$thumb_css_properties."\""; 

			if ( $carousel_class ) {

				$output .= "
			<div class='galleria_thumb $galleryID' $user_css_properties>
				<div class='$carousel_class $galleryID'>
					<div class='jcarousel-container $galleryID'>
						<div class='jcarousel-clip $galleryID'>
							<ul class='galleria_wp $carousel_class  $galleryID' $ul_css_properties>\n";
			} else {
			
			$output .= "
			<div class='galleria_thumb $galleryID' $user_css_properties>
				<ul class='galleria_wp $galleryID' $ul_css_properties>\n";

			}

			break;
		
		case 'right' :

			$user_css_properties = 
					"style=\"float: right; " .$thumb_css_properties."\""; 

			if ( $carousel_class ) {

				$output .= "
			<div class='galleria_thumb $galleryID' $user_css_properties>
				<div class='$carousel_class $galleryID'>
					<div class='jcarousel-container $galleryID'>
						<div class='jcarousel-clip $galleryID'>
							<ul class='galleria_wp $carousel_class  $galleryID' $ul_css_properties>\n";
			} else {
			
			$output .= "
			<div class='galleria_thumb $galleryID' $user_css_properties>
				<ul class='galleria_wp $galleryID' $ul_css_properties>\n";

			}

			break;
		
		default : 
			break;

	} // end of [ switch ( $thumbnail_position ) {} ] 

	return apply_filters( 'gallery_style', $output );

} // end of [ function galleria_generate_pre_list_tags() {} ] 

//==============================================================================//
//   galleria_generate_after_list_tags()                                        //
//==============================================================================//
function galleria_generate_after_list_tags( $galleryID, 
											$thumbnail_position,
											$carousel_class,
											$stage_css_properties ) {

	$output = '';
	$ul_css_properties = '';
	
	switch ( $thumbnail_position ) {
		
		case 'top' :

			if ($stage_css_properties) {
				$stage_css_properties = 
					"style=\"".$stage_css_properties."\"";
			}
			
			if ( $carousel_class ) {
				$output .= "
						</ul>
					</div>
				</div>
			</div>
			<div class='galleria_wp_thumb_caption  $galleryID'>
				<span class='first'></span><span class='msg'></span><span class='last'></span>
			</div>
		</div>\n";

			} else {
			
				$output .= "
					</ul>
				<div class='galleria_wp_thumb_caption  $galleryID'>
					<span class='first'></span><span class='msg'></span><span class='last'></span>
				</div>
		</div>\n";

			}

			$output .= "
		<div id='stage_$galleryID' $stage_css_properties>
			<!-- target image and captions will be inserted here. -->
		</div>\n";

			break;

		case 'bottom' :
		
			if ( $carousel_class ) {
				$output .= "
						</ul>
					</div>
				</div>
			</div>
			<div class='galleria_wp_thumb_caption  $galleryID'>
				<span class='first'></span><span class='msg'></span><span class='last'></span>
			</div>
		</div>\n";

			} else {

				$output .= "
			</ul>
			<div class='galleria_wp_thumb_caption  $galleryID'>
				<span class='first'></span><span class='msg'></span><span class='last'></span>
			</div>
		</div>\n";

			}

			break;

		case 'left' :

			if ( $stage_css_properties ) {
				$stage_css_properties = 
					"style=\"float: right; ".$stage_css_properties."\"";
			}
		
			if ( $carousel_class ) {
				$output .= "
						</ul>
					</div>
				</div>
			</div>
			<div class='galleria_wp_thumb_caption  $galleryID'>
				<span class='first'></span><span class='msg'></span><span class='last'></span>
			</div>
		</div>\n";

			} else {

				$output .= "
				</ul>
			<div class='galleria_wp_thumb_caption  $galleryID'>
				<span class='first'></span><span class='msg'></span><span class='last'></span>
			</div>
		</div>\n";

			}

			$output .= "
		<div id='stage_$galleryID' $stage_css_properties>
			<!-- target image and captions will be inserted here. -->
		</div>
		<p style='clear: right;'></p>\n";

			break;
		
		case 'right' :

			if ( $stage_css_properties ) {
				$stage_css_properties = 
					"style=\"float: left; ".$stage_css_properties."\"";
			}

			if ( $carousel_class ) {
				$output .= "
						</ul>
					</div>
				</div>
			</div>
			<div class='galleria_wp_thumb_caption  $galleryID'>
				<span class='first'></span><span class='msg'></span><span class='last'></span>
			</div>
		</div>\n";

			} else {

				$output .= "
			</ul>
			<div class='galleria_wp_thumb_caption  $galleryID'>
				<span class='first'></span><span class='msg'></span><span class='last'></span>
			</div>
		</div>\n";

			}

			$output .= "
		<div id='stage_$galleryID' $stage_css_properties>
			<!-- target image and captions will be inserted here. -->
		</div>
		<p style='clear: left;'></p>\n";

			break;
		
		default : 
			break;

	} // end of [ switch ( $thumbnail_position ) {} ] 

	return apply_filters( 'gallery_style', $output );

} // end of [ function galleria_generate_after_list_tags() {} ] 

//==============================================================================//
//    function galleria_wp_get_attachment_link():                               //
//==============================================================================//
function galleria_wp_get_attachment_link( $id = 0, 
										  $size = 'thumbnail',
										  $gid = '', 
										  $exif = 0,
										  $exif_tags ) {
	
	global $post;

	$exif_text ='';
	
	$_post = & get_post( intval($id) ); 

	if ( ('attachment' != $_post->post_type) || 
		 !$url = wp_get_attachment_url($_post->ID) ) {
		 
		return __('Missing Attachment','galleria-wp');
	}

	$image_description = '';

	$image_title = attribute_escape($_post->post_title);
	$image_caption = attribute_escape($_post->post_excerpt);
	$image_description = attribute_escape($_post->post_content);

	$groups_text = strstr( $image_title, "[" );
	if ( $groups_text ) {
		$groups_text = str_replace( "[", "", $groups_text );
		$groups_text = str_replace( "]", "", $groups_text );
		$groups_text = trim( $groups_text );
	}

	if ( $gid ) {
		$regexp =  trim( $gid );
		if ( $groups_text ) {
			 if ( ! ereg( $regexp, $groups_text ) ) {
				return '';
			}
		} else {
			return '';
		} // end of [ if ( $groups_text ) ]
	} // end of [ if ( $gid ) ]

	//	invoke bug fixed version of "wp_get_attachment_image()" 
	$link_text = _wp_get_attachment_image( $id, $size );

	$alt_text = 'alt="' . $image_caption . '"';
	$thumb_link = str_replace( 'alt=""', $alt_text, $link_text );

	if ( $exif == 1 ) {
		if ( $url ) {
			$exifdata =	 @ exif_read_data( $url, "EXIF" );
			if ( $exifdata ) {
				$datetime = $exifdata["DateTimeOriginal"];
				$model = $exifdata["Model"];
				$aperture =	 $exifdata["COMPUTED"]["ApertureFNumber"];
				$exposure = $exifdata["ExposureTime"];
				if ( $exif_tags['exif_tag_date'] == 'yes' ) {
					$exif_text .= " ".__('Date','galleria-wp').": $datetime ";
				}
				if ( $exif_tags['exif_tag_aperture'] == 'yes' ) {
					$exif_text .= " ".__('Aperture','galleria-wp').": $aperture ";
				}
				if ( $exif_tags['exif_tag_exposure'] == 'yes' ) {
					$exif_text .= " ".__('Exposure','galleria-wp').": $exposure";
				}
				if ( $exif_tags['exif_tag_model'] == 'yes' ) {
					$exif_text .= " ".__('Model','galleria-wp').": $model";
				}
			}
		}
	}
	if ( $exif_text )  {
		$image_description .= ' :: '.$exif_text;
	}

	return "<a href='$url' title='$image_description'>$thumb_link</a><span>$image_caption</span>";
}

//==============================================================================//
//    function  prepareGalleriaScript():  prepares custom galleria handler      //
//                                                                              //
//    original source code is "demo_01.htm" ( galleria-1.0b demo sample )       //
//==============================================================================//
function  prepareGalleriaScript( $classString,
								 $galleryID,
								 $doShowImage,
								 $isVertical ,
								 $showNavigation = 1,
								 $carousel_class = '',
								 $cropThumbnail = 0,
								 $hideThumbnail= 0,
								 $thumbnailColumns = 0,
								 $thumbnail_caption = '',
								 $playbutton = 0,
								 $interval = 5.0,
								 $shrink = 1,
								 $index = 'top',
								 $dblclick = 'source',
								 $caption1_css_properties,
								 $caption2_css_properties ) {

	$intervalMilliSec = (int) ($interval * 1000);
	$usejcarousel = $carousel_class ? '1' : '0';
	$tempClass = "";
	$output_buffer = "";
	$isVertical = $isVertical ? '1' : '0';

	$output_buffer .= apply_filters( 'gallery_style',"
	var timerID_$galleryID;
	jQuery('ul.$galleryID').galleria( {
		history	  : false,
		clickNext : true, 
		insert	  : '#stage_$galleryID',
		tempClass	: '$galleryID',
		cropThumbnail : '$cropThumbnail',
		shrink	  : '$shrink',
		index	  : '$index',
		doShowImage : '$doShowImage',
		hideThumbnail : '$hideThumbnail',
		isVertical : '$isVertical',
		jcarousel : '$usejcarousel',
		thumbnailColumns : '$thumbnailColumns',
		thumbnail_caption : '$thumbnail_caption',
		caption1_css : '$caption1_css_properties',
		caption2_css : '$caption2_css_properties',
		onImage	  : function( image, caption1, caption2, thumb ) { 
			image.css('display','none').fadeIn(500);
			caption1.css('display','none').fadeIn(50);
			caption2.css('display','none').fadeIn(50);
			var _li = thumb.parents('li');
			_li.siblings().children('img.selected').fadeTo(500,0.5);
			thumb.fadeTo('fast',1).addClass('selected');
			image.attr('title',' Click to Show Next image >> ');
		},
		onThumb : function( thumb ) { 
			var _li = thumb.parents('li');
			var _fadeTo = _li.is('.active') ? '1' : '0.5';
			thumb.css({display:'none',opacity:_fadeTo}).fadeIn(1000);
			thumb.hover(
				function() { thumb.fadeTo('fast',1); },
				function() { _li.not('.active').children('img').fadeTo('fast',0.5); }
				)
			if ( '$dblclick' != 'nothing' ) {
				thumb.bind( 'dblclick', function() {
					var _imgsrc = thumb.attr('rel');
					var _title = thumb.attr('title');
					var _is_ngg = thumb.hasClass('ngg_gallery');
					var _posturl = './' + _imgsrc.split(/\//g).pop().replace(/\...(.)+$/, '/');
					var _openmode = '$dblclick';
					var _dest = ( _openmode.indexOf('post') < 0 || _is_ngg ) ?  _imgsrc : _posturl;
					if ( _openmode.indexOf('current') < 0 ) {
						window.open( _dest );
					} else { 
						location.href = _dest;
					}
				});
			}
		}
	});
	if ( $hideThumbnail == 1 ) {
		// hide thumbnails
		jQuery('ul.$galleryID').css('display', 'none' );
	}
	if ( $showNavigation == 1 ) {
		// show navigation items
		jQuery('div.$galleryID p.galleria_nav').css('display', 'block' );
		//
		if ( $playbutton == 1 ) {
			jQuery('div.$galleryID button.play').css('display', 'block' );
			// automatic navigation 
			jQuery('div.$galleryID button.play').click( function() {
				if ( jQuery(this).text() == '".__('Play','galleria-wp')."' ) {
					jQuery(this).text( '".__('Stop','galleria-wp')."' );
					timerID_$galleryID = setInterval( \"jQuery.galleria.next( 'stage_$galleryID', '$galleryID' );\", $intervalMilliSec );
				} else {
					clearInterval( timerID_$galleryID );
					jQuery(this).text( '".__('Play','galleria-wp')."' );
				}
			} );
		} else {
			// hide the play button
			jQuery('div.$galleryID button.play').css('display', 'none' );
		}
		// add keybord navigation [ May 04th 2008 by Y2 ]
		jQuery(document).keydown( function (e)	{
			if ( e.which == 27 && ($playbutton == 1) ) { // Escape Key
				if ( jQuery('div.$galleryID button.play').text() == '".__('Stop','galleria-wp')."' ) {
					clearInterval( timerID_$galleryID );
					jQuery('div.$galleryID button.play').text( '".__('Play','galleria-wp')."' );
				}
			}
		});
	} // end of	 if ( $navigation == 1 )\n" );
	return $output_buffer;
}

function  preparejCarouselCallback( $numOfGalleries ) {

	global $gGalleryClass;

	$output_buffer = "";

	for ( $i = 1; $i <= $numOfGalleries; $i++ ) {
	
		$galleryID = $gGalleryClass[$i];
		
		$output_buffer .= apply_filters( 'gallery_style',"
var jqc_gallery$i = new Object();
function mycarousel_initCallback_$i( carousel, status ) {
	jqc_gallery$i = carousel;
}
function mycarousel_itemLoadCallback_$i( carousel, status ) {
	var div_caption = jQuery('div.galleria_wp_thumb_caption.$galleryID');
	var span_first  = jQuery('div.galleria_wp_thumb_caption.$galleryID > span.first');
	var span_last   = jQuery('div.galleria_wp_thumb_caption.$galleryID > span.last');
	if ( !carousel.options.vertical ) {
		new_length = carousel.clip.width() + 'px';
		div_caption.css('width', new_length );
		span_first.text(carousel.first);
		span_last.text(carousel.last);
		span_first.css( 'display' , 'block' ).css( 'z-index' , '999' );
		span_first.css( 'background-color' , 'black' ).css( 'color' , 'white' );
		span_first.fadeIn(100);
		span_first.fadeTo(5000,0.5, function() { 
			//jQuery(this).css( 'z-index' , '0' );
			//jQuery(this).css( 'display' , 'none' );
		} ); 
		span_last.css( 'display' , 'block' ).css( 'z-index' , '999' );
		span_last.css( 'color' , 'white' ).css( 'background-color' , 'black' );
		span_last.fadeIn(100);
		span_last.fadeTo(5000,0.5, function() { 
			//jQuery(this).css( 'z-index' , '0' );
			//jQuery(this).css( 'display' , 'none' );
		} ); 
	}
}\n" );
	} // end of [ for ( $i = 1; $i < $numOfGalleries; $i++ ) {} ]

	$output_buffer .= apply_filters( 'gallery_style',"
function jcarousel_scroll_to_active_item( target_class ) {
	var num_of_items =
		parseInt( jQuery('ul.' + target_class + ' li:last-child').attr('jcarouselindex') );
	var jq_active_list = jQuery('ul.' + target_class + ' li.active');
	if ( jq_active_list ) {
		var active_item_index = parseInt( jq_active_list.attr('jcarouselindex') );
		if ( active_item_index > 0	&& 
			 active_item_index <= num_of_items ) {
			switch( target_class ) { \n");

	for ( $i = 1; $i <= $numOfGalleries; $i++ ) {
		$output_buffer .= apply_filters( 'gallery_style',"
				case 'gallery$i' :
					jqc_gallery$i.scroll( active_item_index, true );
					break;
		");
	} // end of [ for ( $i = 1; $i <= $numOfGalleries; $i++ ) ]

	$output_buffer .= apply_filters( 'gallery_style',"
				default:
					break;
			}
		}
	}
}\n" );
	return $output_buffer;
}

function  preparejCarouselScript( $numOfGalleries ) {

	global $gCarouselAttrColumns;
	global $gCarouselAttrSteps;
	global $gCarouselThumbWidth;
	global $gCarouselThumbHeight;
	global $gCarouselIsVertical;
	global $gCarouselAutoHideButton;

	$output_buffer = "";

	for ( $i = 1; $i <= $numOfGalleries; $i++ ) {

		$columns = $gCarouselAttrColumns[$i];
		$steps = $gCarouselAttrSteps[$i];
		$thumb_w = $gCarouselThumbWidth[$i];
		$thumb_h = $gCarouselThumbHeight[$i];
		$vertical_mode = ($gCarouselIsVertical[$i]) ? 'true': 'false';
		$auto_hide = $gCarouselAutoHideButton[$i];

		$output_buffer .= apply_filters( 'gallery_style',"
	jQuery('ul.gallery$i').jcarousel({
			vertical : $vertical_mode,
			visible: $columns,
			scroll:  $steps,
			thumb_w:  $thumb_w,
			thumb_h:  $thumb_h,
			auto_hide: $auto_hide,
			initCallback: mycarousel_initCallback_$i,
			itemLoadCallback: mycarousel_itemLoadCallback_$i
	}); // end of [ jQuery('ul.gallery$i').jcarousel() {} ] \n" );

	} // end of [ for ( $i = 1; $i <= $numOfGalleries; $i++ ) {} ] 
	return $output_buffer;
}

//==========================================================================//
//  following three functions are minor modification version of             //
//  the "/wp-include/media.php"                                             //
//==========================================================================//
function _wp_get_attachment_image( $attachment_id, 
								   $size='thumbnail', 
								   $icon = false ) {
	$html = '';
	$image = _wp_get_attachment_image_src($attachment_id, $size, $icon);
	if ( $image ) {
		list($src, $width, $height) = $image;
		$hwstring = image_hwstring($width, $height);
		if ( is_array($size) )
			$size = join('x', $size);
		$html = '<img src="'.attribute_escape($src).'" 
				'.$hwstring.'class="attachment-'.attribute_escape($size).'" alt="" />';
	}
	return $html;
}
function _wp_get_attachment_image_src( $attachment_id, 
									   $size='thumbnail', 
									   $icon = false ) {
	// get a thumbnail or intermediate image if there is one
	if ( $image = _image_downsize($attachment_id, $size) )
		return $image;
	if ( $icon && $src = wp_mime_type_icon($attachment_id) ) {
		$icon_dir = 
			apply_filters( 'icon_dir', ABSPATH . WPINC . '/images/crystal' );
		$src_file = $icon_dir . '/' . basename($src);
		@list($width, $height) = getimagesize($src_file);
	}
	if ( $src && $width && $height )
		return array( $src, $width, $height );
	return false;
}
function _image_downsize($id, $size = 'medium') {
	if ( !wp_attachment_is_image($id) )
		return false;
	$img_url = wp_get_attachment_url($id);
	$meta = wp_get_attachment_metadata($id);
	$width = $height = 0;
	// plugins can use this to provide resize services
	if ( $out = apply_filters('_image_downsize', false, $id, $size) )
		return $out;
	// try for a new style intermediate size
	if ( $intermediate = image_get_intermediate_size($id, $size) ) {
		$img_url = str_replace(basename($img_url), $intermediate['file'], $img_url);
		$width = $intermediate['width'];
		$height = $intermediate['height'];
	}
	elseif ( $size == 'thumbnail' ) {
		// fall back to the old thumbnail
		$thumb_file = wp_get_attachment_thumb_file( $id ); // modified by Y2
		if ( $info = getimagesize($thumb_file) ) {		   // 
			$img_url = str_replace(basename($img_url), basename($thumb_file), $img_url);
			$width = $info[0];
			$height = $info[1];
		}
	}
	if ( !$width && !$height && isset($meta['width'], $meta['height']) ) {
		// any other type: use the real image and constrain it
		list( $width, $height ) = 
			 image_constrain_size_for_editor( $meta['width'], 
											  $meta['height'], $size );
	}
	if ( $img_url)
		return array( $img_url, $width, $height );
	return false;
}
?>
