=== Galleria WP ===
Contributors: Y2 (Yokoyama Yasuaki)
Donate link: https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=y2%40y2itec%2ecom&item_name=Galleria%20WP&no_shipping=1&no_note=1&tax=0&currency_code=USD&lc=US&bn=PP%2dDonationsBF&charset=UTF%2d8
Tags: plugin, image gallery, slideshow, short code, jQuery, Galleria, jCarousel, NextGEN Gallery
Requires at least: 2.5.0
Tested up to: 2.5.1
Stable tag: 1.2.5

Galleria WP is a multi functional image gallery plugin for WordPress2.5.
Galleria WP uses jQuery Javascript frameworks. (plugin modules: Galleria, jCarousel )

== Description ==

Galleria WP is a image gallery plugin for WordPress2.5 or laters. It is used WordPress 2.5's short code functionality. 

  Galleria  : http://www.monc.se/kitchen/146/galleria-a-javascript-image-gallery
  jCarousel : http://sorgalla.com/jcarousel/
  NextGEN Gallery : http://alexrabe.boelinger.com/wordpress-plugins/nextgen-gallery/

Features:

*   Galleria image gallery
*   Switchable CSS styles
*   Override default CSS styles
*   Show EXIF meta data 
*   Automatic slideshow (play and stop button)
*   Thumnail scroll option (since V1.2)
*   Multiple galleries on a page

== Installation ==

1. Upload `GalleriaWP` directory(folder) to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Set default options ( Enter "Site Admin" page : "Settings" -> "Galleria WP Options" )
4. Upload some images.
5. In your post/page, describe short code tag '[galleria]'. 

   Usage: [galleria <options>] 

        <options> :  orderby, id, size : same as the original [gallery]
                     columns="<number>"  number of thumbnail columns
                     rows="<number>"     number of thumbnail rows    (vertical scroll mode)

                     thumb_w="<number>"  thumbnail width [ pixels ]
                     thumb_h="<number>"  thumbnail height[ pixels ]

                     steps="<number>"    number of thumbnail steps (Prev/Next buttons)
                                         default value is same as the columns
                     gid="<string>"      group tag name (use for  grouping)
                     exif="<0/>1"        showes EXIF meta datas in 2nd caption line
                     cssclass="<string>" style selector for user pre-defined CSS styles
                                         default class is "galleria_std"
                     playbutton="<0/1>"  show automatic slideshow play button
                                         default is playbutton="0" 
                     interval="<number>" display interval of slide show (seconds)
                                         default value is 8.0 [sec]
                     thumbnail="<string>" position of thumbnail block 
                                           'top' 
                                           'bottom'
                                           'left' 
                                           'right'
                                           'top-scroll'
                                           'bottom-scroll'
                                           'left-scroll'
                                           'right-scroll'
                                           'none'  : hide thumbnails
                     navigation="<string>" location of navigation items (prev/next, play buton)
                                           'top'  
                                           'bottom'
                                           'none' : hide navigation(prev/next) items
                     index="<keyword>"   location of image index
                                          'top'   
                                          'bottom'
                                          'caption1'  inject to the first caption line
                                          'caption2'  inject to the second caption line
                                          'none'   hide index
                    shrink="<0/1>"        "1" ( oversized image should be scaled )
                    showimage="<yes/no>"  "yes"
                                          "no" : do not show image (thumbnail only)
                    cropthumb="<yes/no>" thumbnail cropping
                    dblclick="<source/post>"  behavior of double click operation on thumbnail
                                                default : "source"  open original full size image in new window
                                                          "post"    open WP's attachment post page in new window
                                                                    (NextGEN cannot use "post" mode) 
                                                          "current" subordinate keyword  
                                                Examples : 
                                                  "post-current" : open attachment post page in current window
                                                  "current" : (same as "source-current")
                                                              open original full size image in current window

                    background_style="<styles>" overrides default CSS styles (background)
                    caption1_style="<styles>"   overrides default CSS styles (1st caption line)
                    caption2_style="<styles>"   overrides default CSS styles (2nd caption line)
		    stage_style="<styles>"      overrides default stage div CSS styles (main stage)
		    thumbbox_style="<styles>"   overrides default stage div CSS styles (thumbnail box)
                      
                       thumbnail="left-scroll" or "right-scroll :
				 stage_style="width : 75%;"  thumbbox_style="width: 24%;"

                    [ Example ]

			1.  background_style="width: 640px; background-color: #666" 
                            thumbbox_style="width: 512px" 
                            stage_style="width : 512px; background-color: black"                  
                          
                                gallery background color : #666
                                thumnail block width     : 512px 
                                main stage width         : 512px, background color : black
                                
			2.  caption1_style="font-size: 1.2em; font: helvetica; color: #678" 
                    
                   [ Notes ]
                        
                      Default thumbnail size is depend on following CSS properties. 

                      ( Galleria WP V1.2.3 : No need to change CSS settings, you can change thumbnail size 
                        via admin menu. )

                      CSS file : "galleria_wp.css" ( near line #173 - #195 )

			/* default thumbnail size */
			#galleria_wp  .galleria_std  
			div ul li {
				display: block;
				float: left;
				margin: 0;
				overflow: hidden;
				width:  75px;             <=== change here (width)
				height: 75px;             <===             (height)
				border:  2px solid #bbb;
			}
 
			/* default thumbnail size (NextGEN Gallery album's thumbnail)*/
			#galleria_wp  .galleria_std  
				div ul li.ngg_album {
				display: block;
				float: left;
				margin: 0;
				overflow: hidden;
				width: 120px;             <=== change here (width)
				height: 90px;             <===             (height)
				border:  2px solid #bbb;
			}

   

== Screenshots ==

1. default [galleria]  thumbnail size  W75xH75 ( squared & scaled )

       background color : not specified (current background color)
                       
2.  [galleria cssclass="galleria_black" exif="1" playbutton="1"]

       background color : black  ( "galleria_black" is defined in "galleria.css" )
       EXIF meta data will be displayed in 2nd caption line(if image contains EXIF data)
       Automatic slideshow "Play" button will be appeared.
 
3.  [galleria exif="1" ]

    Galleria WP plugin samples
      
       Standard  (No options) : 
           http://y2itec.com/blog/inet/wp/galleria-wp-plugin-1-798/
       
       Sample #2  (grouping, EXIF, slideshow)
           http://y2itec.com/blog/inet/wp/galleria-wp-plugin-2-808/
       
       Sample #3  (V1.2 : thumbnail scroll option)
           http://y2itec.com/blog/inet/wp/galleria-wp-844/

== Frequently Asked Questions == 

= How do I use "gid" grouping option? =

You need add group tags with bracket to "Title" field of the media upload dialog.
  
  Photo #1  Title: Bob [pet cat persian]
  Photo #2  Title: Nimo[pet fish clown anemonefish ]
  Photo #3  Title: Alice[friends female San Francisco CA US]
  Photo #4  Title: Without group tags
  
  If you give a group option [galleria gid="pet"], only #1 and #2 will be displayed.
  
= Can I contain multiple [galleria] short code on a page? =

YES.  Galleria WP V1.1(or laters) can display multiple galleries on a page.
      Restrictions : gallery use thumnail scroll options ( thumbnail = "top-scroll" or "bottom-scroll" )
                     should be placed the last position, otherwise automatic thumnail scroll will not work.
      
== Changelog ==

1.2.5 bug fixed(revised javascript loading method) Jun 23 2008

1.2.4 bug fixed Jun 22 2008

1.2.3 added user's default options admin menu. Jun 21 2008

1.2.2 revised double click operation handler code and added langage resources. Jun 15 2008

1.2.1 bug fixed (ghost caption problem and improved double clicking operation) Jun 13 2008
      Special  thank to alex. [http://www.alex-betty.com/]

1.2.0 added thumbnail scroll option and thumbnail position (left, right) Jun 9 2008
      Special thanks to Mr. Papa. [ http://cruisetalk.org/ ]

1.1.2 added thumbnail background style option   May 18 2008

1.1.1 added thumbnail 'none' option   May 18 2008

1.1.0 beta test release      May 13 2008

1.0.3  added shrink option    May 10 2008

1.0.2  adjust for Opera browser   May 08 2008

1.0.1  minor bug fix    May 08 2008 

1.0.0  first release version    May 07 2008 
