<?php
//==============================================================================//
//   file :  galleria-wp-admin.php   Galleria WP Default Options (Admin Menu)   //
//==============================================================================//
if(preg_match('#' . basename(__FILE__) . '#', $_SERVER['PHP_SELF'])) { 
	die('Illegal Entry'); 
}

function galleria_wp_admin_options_handler() {

	$galleria_option_id = 'galleria_wp_options';
	$hidden_field_name  = 'galleria_wp_options_hidden';

	$default_options = array(
		'orderby'	 => 'menu_order ASC, ID ASC',
		'size'		 => 'thumbnail',
		'columns'	 => 0,
		'rows'		 => 0,
		'exif'		 => 'no',
		'playbutton' => 'no',
		'interval'	 => 8.0,
		'shrink'	 => 'yes',
		'dblclick'	 => 'source',
		'showimage'	 => 'yes',
		'navigation' => 'bottom',
		'index'		 => 'bottom',
		'thumbnail'	 => 'top-scroll',
		'thumb_w'	 => 0,
		'thumb_h'	 => 0,
		'steps'		 => 0,
		'cropthumb'	 => 'no',
		'cssclass'	 => 'galleria_std',
		'background_style' => '',
		'thumbbox_style'   => '',
		'stage_style'	   => '',
		'caption1_style'   => '',
		'caption2_style'   => '',
		'archives'         => 'silent',
		'auto_hide'        => 'yes',
		'thumb_top_css'	   => '',
		'thumb_bottom_css' => '',
		'thumb_left_css'   => 'width: 19%;',
		'thumb_right_css'  => 'width: 19%;',
		'stage_top_css'	   => '',
		'stage_bottom_css' => '',
		'stage_left_css'   => 'width: 80%;',
		'stage_right_css'  => 'width: 80%;',
		'exif_tag_date'		=> 'yes',
		'exif_tag_aperture' => 'yes',
		'exif_tag_exposure' => 'yes',
		'exif_tag_model'	=> 'yes',
		'thumb_position'    => '',
		'thumb_mode    '    => '',
		'dblclick_source'   => 'source',
		'dblclick_window'   => 'new'
	);

	//check to see if user's default setting is exist or not
	if ( !get_option( $galleria_option_id ) ) {
		// set initial value
		update_option( $galleria_option_id, $default_options );
	}

	// Read in existing option value from database
	$default_options = (array) get_option( $galleria_option_id );

	//--------------------------------------------//
	$default_options['thumb_mode'] = 
		strstr( $default_options['thumbnail'], 'scroll' ) ? "scroll" : "tile";
	
	if ( strstr( $default_options['thumbnail'], 'top' ) ) {
		$default_options['thumb_position'] = "top";
	} else if ( strstr( $default_options['thumbnail'], 'bottom' ) ) {
		$default_options['thumb_position'] = "bottom";
	} else if ( strstr( $default_options['thumbnail'], 'left' ) ) {
		$default_options['thumb_position'] = "left";
	} else if ( strstr( $default_options['thumbnail'], 'right' ) ) {
		$default_options['thumb_position'] = "right";
	} else if ( strstr( $default_options['thumbnail'], 'none' ) ) {
		$default_options['thumb_position'] = "none";
	} else {
		$default_options['thumb_position'] = "top";
	}
	if ( strstr($default_options['dblclick'], 'source' ) ) {
		$default_options['dblclick_source'] = "source";
	} else if ( strstr($default_options['dblclick'], 'post' ) ) {
		$default_options['dblclick_source'] = "post";
	} else {
		$default_options['dblclick_source'] = "nothing";
	}
	$default_options['dblclick_window'] = 
		strstr( $default_options['dblclick'], 'current' ) ? "current" : "new";

	// See if the user has posted us some information
	// If they did, this hidden field will be set to 'Y'
	if( $_POST[ $hidden_field_name ] == 'Y' ) {

		$default_options['thumb_mode']     = $_POST['thumb_mode'];
		$default_options['thumb_position'] = $_POST['thumb_position'];
		
		$default_options['thumbnail'] = 
			( $default_options['thumb_mode'] == "scroll" ) ?  
				$default_options['thumb_position']."-scroll" : 
				$default_options['thumb_position'];

		$default_options['dblclick_source'] = $_POST['dblclick_source'];
		$default_options['dblclick_window'] = $_POST['dblclick_window'];
		
		if ( strstr( $default_options['dblclick_source'], 'source' ) ) {
			$default_options['dblclick'] = "source";

		} else if ( strstr( $default_options['dblclick_source'], 'post' ) ) {
			$default_options['dblclick'] = "post";
		} else {
			$default_options['dblclick'] = "nothing";
		}
		if ( $default_options['dblclick'] != "nothing" ) {
			$default_options['dblclick'] .= 
				strstr( $default_options['dblclick_window'], 'current' ) ? 
					"-current" : "-new";
		}
	
		$default_options['cropthumb'] = $_POST['cropthumb'];
		$default_options['size']      = $_POST['size'];
		$default_options['auto_hide'] = $_POST['auto_hide'];

		$default_options['thumb_w'] = intval($_POST['thumb_w']);
		$default_options['thumb_h'] = intval($_POST['thumb_h']);
		$default_options['columns'] = intval($_POST['columns']);
		$default_options['steps']   = intval($_POST['steps']);
		
		$default_options['showimage']  = $_POST['showimage'];
		$default_options['shrink']     = $_POST['shrink'];

		$default_options['navigation'] = $_POST['navigation'];
		$default_options['index']      = $_POST['index'];
		$default_options['exif']       = $_POST['exif'];
		$default_options['exif_tag_date']     = $_POST['exif_tag_date'];
		$default_options['exif_tag_aperture'] = $_POST['exif_tag_aperture'];
		$default_options['exif_tag_exposure'] = $_POST['exif_tag_exposure'];
		$default_options['exif_tag_model']    = $_POST['exif_tag_model'];
		$default_options['playbutton'] = $_POST['playbutton'];
		$default_options['interval']   = (double) $_POST['interval'];

		$default_options['cssclass']   = trim($_POST['cssclass']);

		$default_options['thumb_top_css']    = trim($_POST['thumb_top_css']);
		$default_options['thumb_bottom_css'] = trim($_POST['thumb_bottom_css']);
		$default_options['thumb_left_css']   = trim($_POST['thumb_left_css']);
		$default_options['thumb_right_css']  = trim($_POST['thumb_right_css']);
		$default_options['stage_top_css']    = trim($_POST['stage_top_css']);
		$default_options['stage_bottom_css'] = trim($_POST['stage_bottom_css']);
		$default_options['stage_left_css']   = trim($_POST['stage_left_css']);
		$default_options['stage_right_css']  = trim($_POST['stage_right_css']);
		$default_options['caption1_style']   = trim($_POST['caption1_style']);
		$default_options['caption2_style']   = trim($_POST['caption2_style']);

		update_option( $galleria_option_id, $default_options );

?>
<div class="updated">
	<p>
		<strong><?php _e('Options saved.', 'galleria-wp'); ?></strong>
	</p>
</div>
<?php
	} // end if [ if( $_POST[ $hidden_field_name ] == 'Y' ) {} ]
	echo '<div class="galleria_menu_wrapper" style="margin-left: 2em;">';
	echo "<h2>" . __( 'Galleria WP Default Options', 'galleria-wp') . "</h2>";
?>

	<form name="form1" method="post" 
		action="<?php echo str_replace( '%7E', '~', $_SERVER['REQUEST_URI']); ?>">
		<input type="hidden" name="<?php echo $hidden_field_name; ?>" value="Y">

<!-- submit button -->
		<p class="submit">
			<input type="submit" name="Submit" 
				value="<?php _e('Update Options', 'galleria-wp') ?>" 
			/>
		</p>

		<hr />

<!-- Thumbnail Options -->
		<h3><?php _e("Thumbnail", 'galleria-wp'); ?> </h3>

		<div>
			<span style="display: block;float: left;  width: 12em;" >
				<?php _e("Mode :", 'galleria-wp'); ?>
			</span>
			<span style="display: block;float: left;  width: 10em;">
				<input type="radio" name="thumb_mode" value="scroll" 
					<?php if ( strstr($default_options['thumbnail'], "scroll") ) { echo 'CHECKED'; } ?> /><?php _e("Scroll", 'galleria-wp'); ?>
			</span>
			<span style="display: block;float: left;  width: 10em;">
				<input type="radio" name="thumb_mode" value="tile" 
					<?php if ( !strstr( $default_options['thumbnail'], "scroll" ) ) { echo 'CHECKED'; } ?> /><?php _e("Tile", 'galleria-wp'); ?>
			</span>
			<p style="clear: both;"></p>
		</div>

		<div style="display: block;">
			<span style="display: block;float: left;  width: 12em;" >
				<?php _e("Position :", 'galleria-wp'); ?>
			</span>
			<span style="display: block;float: left;  width: 10em;">
				<input type="radio" name="thumb_position" value="top" 
					<?php if ( strstr($default_options['thumb_position'], "top") ) { echo 'CHECKED'; } ?> 
				/><?php _e("Top", 'galleria-wp'); ?>
			</span>
			<span style="display: block;float: left;  width: 10em;">
				<input type="radio" name="thumb_position" value="bottom" 
					<?php if ( strstr($default_options['thumb_position'], "bottom") ) { echo 'CHECKED'; } ?> 
				/><?php _e("Bottom", 'galleria-wp'); ?>
			</span>
			<span style="display: block;float: left;  width: 10em;">
				<input type="radio" name="thumb_position" value="left" 
					<?php if ( strstr($default_options['thumb_position'], "left") ) { echo 'CHECKED'; } ?> 
				/><?php _e("Left", 'galleria-wp'); ?>
			</span>
			<span style="display: block;float: left;  width: 10em;">
				<input type="radio" name="thumb_position" value="right" 
					<?php if ( strstr($default_options['thumb_position'], "right") ) { echo 'CHECKED'; } ?> 
				/><?php _e("Right", 'galleria-wp'); ?>
			</span>
			<span style="display: block;float: left;  width: 10em;">
				<input type="radio" name="thumb_position" value="none" 
					<?php if ( strstr($default_options['thumb_position'], "none") ) { echo 'CHECKED'; } ?> 
				/><?php _e("None", 'galleria-wp'); ?>
			</span>
			<p style="clear: both;"></p>
		</div>
		
		<div style="display: block;">
			<span style="display: block;float: left;  width: 12em;" >
				<?php _e("Scaling Mode :", 'galleria-wp'); ?>
			</span>
			<span style="display: block;float: left;  width: 10em;">
				<input type="radio" name="cropthumb" value="no"  
							<?php if ( strstr($default_options['cropthumb'], "no") ) { echo 'CHECKED'; } ?> 
				/><?php _e("Scale", 'galleria-wp'); ?>
			</span>
			<span style="display: block;float: left;  width: 10em;">
				<input type="radio" name="cropthumb" value="yes"	 
					<?php if ( strstr($default_options['cropthumb'], "yes") ) { echo 'CHECKED'; } ?> 
				/><?php _e("Crop", 'galleria-wp'); ?>
			</span>
			<p style="clear: both;"></p>
		</div>

		<div style="display: block;">
			<span style="display: block;float: left;  width: 12em;" >
				<?php _e("Thumbnail Source :", 'galleria-wp'); ?>
			</span>
			<span style="display: block;float: left;  width: 10em;">
				<input type="radio" name="size" value="thumbnail"  
					<?php if ( strstr($default_options['size'], "thumbnail") ) { echo 'CHECKED'; } ?> 
				/><?php _e("thumbnail", 'galleria-wp'); ?>
			</span>
			<span style="display: block;float: left;  width: 10em;">
				<input type="radio" name="size" value="medium"	
					<?php if ( strstr($default_options['size'], "medium") ) { echo 'CHECKED'; } ?> 
				/><?php _e("medium", 'galleria-wp'); ?>
			</span>
			<p style="clear: both;"></p>
		</div>
		
		<div style="display: block;">
			<span style="display: block;float: left;  width: 12em;" >
				<?php _e("Prev/Next Button :", 'galleria-wp'); ?>
			</span>
			<span style="display: block;float: left;  width: 10em;">
				<input type="radio" name="auto_hide" value="yes"  
					<?php if ( strstr($default_options['auto_hide'], "yes") ) { echo 'CHECKED'; } ?> 
				/><?php _e("Automatic", 'galleria-wp'); ?>
			</span>
			<span style="display: block;float: left;  width: 10em;">
				<input type="radio" name="auto_hide" value="no" 
					<?php if ( strstr($default_options['auto_hide'], "no") ) { echo 'CHECKED'; } ?> 
				/><?php _e("Always Show", 'galleria-wp'); ?>
			</span>
			<p style="clear: both;"></p>
		</div>

		<div style="display: block;">
			<h5 style="display: block;float: left;  width: 16em;" >
				<?php _e("Double Click Operation Mode", 'galleria-wp'); ?>
			</h5>
			<p style="clear: both;"></p>
			
			<span style="display: block;float: left;  width: 12em; text-indent: 1em;" >
				<?php _e("Source :", 'galleria-wp'); ?>
			</span>
			<span style="display: block;float: left;  width: 10em;">
				<input type="radio" name="dblclick_source" value="source" 
					<?php if ( strstr($default_options['dblclick_source'], "source") ) { echo 'CHECKED'; } ?> 
				/><?php _e("Original Image", 'galleria-wp'); ?>
			</span>
			<span style="display: block;float: left;  width: 20em;">
				<input type="radio" name="dblclick_source" value="post"	
					<?php if ( strstr($default_options['dblclick_source'], "post") ) { echo 'CHECKED'; } ?> 
				/><?php _e("Open Attachement Post Page", 'galleria-wp'); ?>
			</span>
			<span style="display: block;float: left;  width: 10em;">
				<input type="radio" name="dblclick_source" value="nothing"	
					<?php if ( strstr($default_options['dblclick_source'], "nothing") ) { echo 'CHECKED'; } ?> 
				/><?php _e("Nothing", 'galleria-wp'); ?>
			</span>
			<p style="clear: both;"></p>
		</div>
		<br />
		<div style="display: block;">
			<span style="display: block;float: left;  width: 12em; text-indent: 1em;" >
				<?php _e("Destination :", 'galleria-wp'); ?>
			</span>
			<span style="display: block;float: left;  width: 10em;">
				<input type="radio" name="dblclick_window" value="new"  
					<?php if ( strstr($default_options['dblclick_window'], "new") ) { echo 'CHECKED'; } ?> 
				/><?php _e("New Window", 'galleria-wp'); ?> 
			</span>
			<span style="display: block;float: left;  width: 20em;">
				<input type="radio" name="dblclick_window" value="current"	
					<?php if ( strstr($default_options['dblclick_window'], "current") ) { echo 'CHECKED'; } ?> 
				/><?php _e("Current Window", 'galleria-wp'); ?>
			</span>
			<p style="clear: both;"></p>
		</div>
		<br />
		<div style="display: block; ">
			<h5 style="display: block;float: left;  width: 80em;" >
				<?php _e("Thumbnail Size and Columns/Rows [ leave zero to use default settings ]", 'galleria-wp'); ?>
			</h5>
			<p style="clear: both;"></p>
			<span style="display: block;float: left;  width: 12em;  text-indent: 1em;" >
				<?php _e("Size :", 'galleria-wp'); ?>
			</span>
			<span style="display: block;float: left;  width: 10em;" >
				<?php _e("Width :", 'galleria-wp'); ?>
				<input type="text" name="thumb_w" size="3" value="<?php echo $default_options['thumb_w'] ?>" />
			</span>
			<span style="display: block;float: left;  width: 10em;" >
				<?php _e("Height :", 'galleria-wp'); ?>
				<input type="text" name="thumb_h" size="3" value="<?php echo $default_options['thumb_h'] ?>" />
			</span>
			<span style="display: block;float: left;  width: 8em; margin: 0.4em; 0" >
				<?php _e("[ pixels ]", 'galleria-wp'); ?>
			</span>
			<p style="clear: both;"></p>
		</div>
		<div style="display: block; ">
			<span style="display: block;float: left;  width: 12em;  text-indent: 1em;" >
				<?php _e("Others :", 'galleria-wp'); ?>
			</span>
			<span style="display: block;float: left;  width: 12em;" >
				<?php _e("Columns(Rows) :", 'galleria-wp'); ?>
				<input type="text" name="columns" size="2" value="<?php echo $default_options['columns'] ?>" />
			</span>
			<span style="display: block;float: left;  width: 17em;" >
				<?php _e("Steps :", 'galleria-wp'); ?>
				<input type="text" name="steps" size="2" value="<?php echo $default_options['steps'] ?>" />
			</span>
			<p style="clear: both;"></p>
			
		</div>

		<hr />

<!-- Image Options -->
		<h3><?php _e("Main Image", 'galleria-wp'); ?> </h3>
		<div style="display: block;">
			<span style="display: block;float: left;  width: 12em;" >
				<?php _e("Display Mode :", 'galleria-wp'); ?>
			</span>
			<span style="display: block;float: left;  width: 10em;" >
				<input type="radio" name="showimage" value="yes" 
					<?php if ( strstr($default_options['showimage'], "yes") ) { echo 'CHECKED'; } ?> 
					/><?php _e("Show", 'galleria-wp'); ?>
			</span>
			<span style="display: block;float: left;  width: 10em;" >
				<input type="radio" name="showimage" value="no" 
					<?php if ( strstr($default_options['showimage'], "no") ) { echo 'CHECKED'; } ?> 
					/><?php _e("Hidden", 'galleria-wp'); ?>
			</span>
			<p style="clear: both;"></p>
		</div>
		<div style="display: block;">
			<span style="display: block;float: left;  width: 12em;" >
				<?php _e("Shrink Mode :", 'galleria-wp'); ?>
			</span>
			<span style="display: block;float: left;  width: 10em;" >
				<input type="radio" name="shrink" value="yes" 
					<?php if ( strstr($default_options['shrink'], "yes") ) { echo 'CHECKED'; } ?> 
				/><?php _e("Shrink", 'galleria-wp'); ?>
			</span>
			<span style="display: block;float: left;  width: 10em;" >
				<input type="radio" name="shrink" value="no" 
					<?php if ( strstr($default_options['shrink'], "no") ) { echo 'CHECKED'; } ?> 
				/><?php _e("Crop", 'galleria-wp'); ?>
			</span>
			<p style="clear: both;"></p>
		</div>

		<hr />

<!-- Other Options -->
		<h3><?php _e("Others", 'galleria-wp'); ?> </h3>
		<div style="display: block;">
			<span style="display: block;float: left;  width: 12em;" >
				<?php _e("Navigation :", 'galleria-wp'); ?>
			</span>
			<span style="display: block;float: left;  width: 10em;" >
				<input type="radio" name="navigation" value="top" 
					<?php if ( strstr($default_options['navigation'], "top") ) { echo 'CHECKED'; } ?> 
				/><?php _e("Top", 'galleria-wp'); ?>
			</span> 
			<span style="display: block;float: left;  width: 10em;" >
				<input type="radio" name="navigation" value="bottom" 
					<?php if ( strstr($default_options['navigation'], "bottom") ) { echo 'CHECKED'; } ?> 
				/><?php _e("Bottom", 'galleria-wp'); ?>
			</span>
			<span style="display: block;float: left;  width: 10em;" >
				<input type="radio" name="navigation" value="none" 
					<?php if ( strstr($default_options['navigation'], "none") ) { echo 'CHECKED'; } ?> 
				/><?php _e("None", 'galleria-wp'); ?>
			</span>
			<p style="clear: both;"></p>
		</div>
		<div style="display: block;">
			<span style="display: block;float: left;  width: 12em;" >
				<?php _e("Image Index :", 'galleria-wp'); ?>
			</span>
			<span style="display: block;float: left;  width: 10em;" >
				<input type="radio" name="index" value="top" 
					<?php if ( strstr($default_options['index'], "top") ) { echo 'CHECKED'; } ?> 
				/><?php _e("Top", 'galleria-wp'); ?>
			</span>
			<span style="display: block;float: left;  width: 10em;" >
				<input type="radio" name="index" value="bottom" 
					<?php if ( strstr($default_options['index'], "bottom") ) { echo 'CHECKED'; } ?> 
				/><?php _e("Bottom", 'galleria-wp'); ?>
			</span>
			<span style="display: block;float: left;  width: 10em;" >
				<input type="radio" name="index" value="caption1" 
					<?php if ( strstr($default_options['index'], "caption1") ) { echo 'CHECKED'; } ?> 
				/><?php _e("Caption 1", 'galleria-wp'); ?>
			</span>
			<span style="display: block;float: left;  width: 10em;" >
				<input type="radio" name="index" value="caption2" 
					<?php if ( strstr($default_options['index'], "caption2") ) { echo 'CHECKED'; } ?> 
				/><?php _e("Caption 2", 'galleria-wp'); ?>
			</span>
			<span style="display: block;float: left;  width: 10em;" >
				<input type="radio" name="index" value="none" 
					<?php if ( strstr($default_options['index'], "none") ) { echo 'CHECKED'; } ?> 
				/><?php _e("None", 'galleria-wp'); ?>
			</span>
			<p style="clear: both;"></p>
		</div>
		<br />
		<div style="display: block;">
			<span style="display: block;float: left;  width: 12em;" >
				<?php _e("EXIF Data :", 'galleria-wp'); ?>
			</span>
			<span style="display: block;float: left;  width: 10em;" >
				<input type="radio" name="exif" value="yes" 
					<?php if ( strstr($default_options['exif'], "yes") ) { echo 'CHECKED'; } ?> 
				/><?php _e("Show", 'galleria-wp'); ?>
			</span>
			<span style="display: block;float: left;  width: 10em;" >
				<input type="radio" name="exif" value="no" 
					<?php if ( strstr($default_options['exif'], "no") ) { echo 'CHECKED'; } ?> 
				/><?php _e("Hide", 'galleria-wp'); ?>
			</span>
			<span style="display: block;float: left;  width: 12em;" >
				<?php _e("Tags: ", 'galleria-wp'); ?>
				<input type="checkbox" name="exif_tag_date" value="yes" 
					<?php if ( strstr($default_options['exif_tag_date'], "yes") ) { echo 'CHECKED'; } ?> 
				/><?php _e("Date", 'galleria-wp'); ?>
			</span>
			<span style="display: block;float: left;  width: 8em;" >
				<input type="checkbox" name="exif_tag_aperture" value="yes" 
					<?php if ( strstr($default_options['exif_tag_aperture'], "yes") ) { echo 'CHECKED'; } ?> 
				/><?php _e("Aperture", 'galleria-wp'); ?>
			</span>
			<span style="display: block;float: left;  width: 8em;" >
				<input type="checkbox" name="exif_tag_exposure" value="yes"
					<?php if ( strstr($default_options['exif_tag_exposure'], "yes") ) { echo 'CHECKED'; } ?> 
				/><?php _e("Exposure", 'galleria-wp'); ?>
			</span>
			<span style="display: block;float: left;  width: 8em;" >
				<input type="checkbox" name="exif_tag_model" value="yes"
					<?php if ( strstr($default_options['exif_tag_model'], "yes") ) { echo 'CHECKED'; } ?> 
				/><?php _e("Model", 'galleria-wp'); ?>
			</span>
			<p style="clear: both;"></p>
		</div>
		<br />
		<div style="display: block; ">
			<span style="display: block;float: left;  width: 12em; margin: 0.4em	 0;" >
				<?php _e("Play Button :", 'galleria-wp'); ?>
			</span>
			<span style="display: block;float: left;  width: 10em;" >
				<input type="radio" name="playbutton" value="yes" 
					<?php if ( strstr($default_options['playbutton'], "yes") ) { echo 'CHECKED'; } ?> 
				/><?php _e("Show", 'galleria-wp'); ?>
			</span>
			<span style="display: block;float: left;  width: 10em;" >
				<input type="radio" name="playbutton" value="no"  
					<?php if ( strstr($default_options['playbutton'], "no") ) { echo 'CHECKED'; } ?> 
				/><?php _e("Hide", 'galleria-wp'); ?>
			</span>
			<span style="display: block;float: left;  width: 10em;" >
				<?php _e("Interval :", 'galleria-wp'); ?>
					<input type="text" name="interval" size="5" 
						value="<?php echo $default_options['interval'] ?>" />
			</span>
					<span style="display: block;float: left;  width: 8em; margin: 0.4em; 0" >
						<?php _e("[ sec. ]", 'galleria-wp'); ?>
					</span>
			<p style="clear: both;"></p>
		</div>

		<hr />
		<h3><?php _e("Default CSS Template class", 'galleria-wp'); ?> </h3>
		<div style="display: block; ">
			<span style="display: block;float: left;  width: 12em; " >
				<?php _e("Template Class :", 'galleria-wp'); ?>
			</span>
			<span style="display: block;float: left;  width: 50em;" >
				<input type="text" name="cssclass" size="40" 
					value="<?php echo trim($default_options['cssclass']) ?>" 
				/>
			</span>
			<p style="clear: both;"></p>
		</div>
		<hr />

		<h3><?php _e("Individual CSS Styles", 'galleria-wp'); ?> </h3>
		<div style="display: block; ">
 
			<h4 style="display: block;float: left;	width: 12em; " >
				<?php _e("Thumbnail", 'galleria-wp'); ?>
			</h4>
			<p style="clear: both;"></p>
			<span style="display: block;float: left;  width: 12em; " >
				<?php _e("Top :", 'galleria-wp'); ?>
			</span>
			<span style="display: block;float: left;" >
				<input type="text" name="thumb_top_css" size="80" 
					value="<?php echo trim($default_options['thumb_top_css']) ?>" 
				/>
			</span>
			<p style="clear: both;"></p>
			<span style="display: block;float: left;  width: 12em; " >
				<?php _e("Bottom :", 'galleria-wp'); ?>
			</span>
			<span style="display: block;float: left;" >
				<input type="text" name="thumb_bottom_css" size="80" 
					value="<?php echo trim($default_options['thumb_bottom_css']) ?>" 
				/>
			</span>
			<p style="clear: both;"></p>
			<span style="display: block;float: left;  width: 12em; " >
				<?php _e("Left :", 'galleria-wp'); ?>
			</span>
			<span style="display: block;float: left;" >
				<input type="text" name="thumb_left_css" size="80" 
					value="<?php echo trim($default_options['thumb_left_css']) ?> " 
				/>
			</span>
			<p style="clear: both;"></p>
			<span style="display: block;float: left;  width: 12em; " >
				<?php _e("Right :", 'galleria-wp'); ?>
			</span>
			<span style="display: block;float: left;" >
				<input type="text" name="thumb_right_css" size="80" 
					value="<?php echo trim($default_options['thumb_right_css']) ?>" 
				/>
			</span>
			<p style="clear: both;"></p>

			<h4 style="display: block;float: left;	width: 12em; " >
				<?php _e("Main Image", 'galleria-wp'); ?>
			</h4>
			<p style="clear: both;"></p>

			<span style="display: block;float: left;  width: 12em; " >
				<?php _e("Top :", 'galleria-wp'); ?>
			</span>
			<span style="display: block;float: left;" >
				<input type="text" name="stage_top_css" size="80" 
					value="<?php echo trim($default_options['stage_top_css']) ?>"
				/>
			</span>
			<p style="clear: both;"></p>
			<span style="display: block;float: left;  width: 12em; " >
				<?php _e("Bottom :", 'galleria-wp'); ?>
			</span>
			<span style="display: block;float: left;" >
				<input type="text" name="stage_bottom_css" size="80" 
					value="<?php echo trim($default_options['stage_bottom_css']) ?>"
				/>
			</span>
			<p style="clear: both;"></p>
			<span style="display: block;float: left;  width: 12em; " >
				<?php _e("Left :", 'galleria-wp'); ?>
			</span>
			<span style="display: block;float: left;" >
				<input type="text" name="stage_left_css" size="80" 
					value="<?php echo trim($default_options['stage_left_css']) ?>"
				/>
			</span>
			<p style="clear: both;"></p>
			<span style="display: block;float: left;  width: 12em; " >
				<?php _e("Right :", 'galleria-wp'); ?>
			</span>
			<span style="display: block;float: left;" >
				<input type="text" name="stage_right_css" size="80" 
					value="<?php echo trim($default_options['stage_right_css']) ?>"
				/>
			</span>
			<p style="clear: both;"></p>

			<h4 style="display: block;float: left;	width: 12em; " >
				<?php _e("Caption", 'galleria-wp'); ?>
			</h4>
			<p style="clear: both;"></p>

			<span style="display: block;float: left;  width: 12em; " >
				<?php _e("Caption 1 :", 'galleria-wp'); ?>
			</span>
			<span style="display: block;float: left;" >
				<input type="text" name="caption1_style" size="80" 
					value="<?php echo trim($default_options['caption1_style']) ?>"
				/>
			</span>
			<p style="clear: both;"></p>
			<span style="display: block;float: left;  width: 12em; " >
				<?php _e("Caption 2 :", 'galleria-wp'); ?>
			</span>
			<span style="display: block;float: left;" >
				<input type="text" name="caption2_style" size="80" 
					value="<?php echo trim($default_options['caption2_style']) ?>"
				/>
			</span>
			<p style="clear: both;"></p>
		</div>
		<hr />

<!-- submit button -->
		<p class="submit">
			<input type="submit" name="Submit" 
				value="<?php _e('Update Options', 'galleria-wp') ?>" 
			/>
		</p>

	</form>

</div> <!-- [ div class="galleria_menu_wrapper" ] -->

<?php
}
?>