<?php

/**
 * Backup Module for Ready Backup plugin
 * @package ReadyBackup\Modules\Backup
 * @version 2.0
 * @author  Artur Kovalevsky
 */
class backupBup extends moduleBup {
	
	/**
	 * Path to libraries
	 * @var string
	 */
	private $_librariesPath = 'classes';
	
	/**
	 * Libraries list
	 * @var array
	 */
	private $_requiredLibraries = array(
		'Zip.php', 'pclzip.lib.php',
	);
	
	/**
	 * Menu tab configuration
	 * @var array
	 */
	private $tab = array(
		'key'    => 'bupStorageOptions',
		'title'  => 'Backups+',
		'action' => 'indexAction',
	);
	
	/**
	 * Plugin initialization
	 */
	public function init() {
		parent::init();
		
		add_action('plugins_loaded', array($this, 'loadModuleLibraries'));
		add_action('plugins_loaded', array($this, 'loadModuleScripts'));
		
		dispatcherBup::addFilter('adminOptionsTabs', array($this, 'registerModuleTab'));
		
		// Force run download action if $_GET param setted
		if (isset($_GET['download']) && !empty($_GET['download'])) {
			$this->run('downloadAction');
		}
	}
	
	/**
	 * Load module dependencies & classes
	 */
	public function loadModuleLibraries() {
		if (!empty($this->_requiredLibraries) && is_array($this->_requiredLibraries)) {
			
			$libPath = str_replace('/', '', $this->_librariesPath);
			$path = realpath($this->getModDir()) . DIRECTORY_SEPARATOR . $libPath;
			if (!is_dir($path)) {
				define('BUP_MODBACKUP_DEPENDENCE', false);
				return false;
			}
			
			foreach ($this->_requiredLibraries as $library) {
				if (file_exists($path . DIRECTORY_SEPARATOR . $library)) {
					require_once $path . DIRECTORY_SEPARATOR . $library;
				}
				else {
					if (!defined('BUP_MODBACKUP_DEPENDENCE')) {
						define('BUP_MODBACKUP_DEPENDENCE', false);
						return false;
					}
				}
			}
			
			if (!defined('BUP_MODBACKUP_DEPENDENCE')) {
				define('BUP_MODBACKUP_DEPENDENCE', true);
				return true;
			}
		}
		else {
			define('BUP_MODBACKUP_DEPENDENCE', false);
			return false;
		}
	}
	
	/**
	 * Load javascript & css files
	 */
	public function loadModuleScripts() {
		if (is_admin()) {
			frameBup::_()->addScript('adminBackupOptionsV2', $this->getModPath() . 'js/admin.backup.v2.js');
		}
	}
	
	/**
	 * Add tab to the menu
	 */
	public function registerModuleTab($tabs) {
		$tabs[$this->tab['key']] = array(
			'title'   => $this->tab['title'],
			'content' => $this->run($this->tab['action']),
		);
		
		return $tabs;
	}
	
    /**
     * Run controller's action
     */
    public function run($action) {
        $controller = $this->getController();
        if(method_exists($controller, $action)) {
            return $controller->{$action}();
        }
    }
}