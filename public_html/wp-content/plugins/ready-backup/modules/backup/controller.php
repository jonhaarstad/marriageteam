<?php

/**
 * Backup Module for Ready Backup
 * @package ReadyBackup\Modules\Backup
 * @version 2.0
 * @author  Artur Kovalevsky
 */
class backupControllerBup extends controllerBup {
	
	public function indexAction() {
		$model     = $this->getModel();
		$backups   = $model->getBackupsList();
		$providers = array();
		
		$old = $model->getOldBackupsList();
		
		return $this->render('index', array(
			'backups'   => $backups,
			'old'       => $old,
			'providers' => dispatcherBup::applyFilters('adminSendToLinks', $providers),
		));
	}
	
	/**
	 * Create Action
	 * Create new backup
	 */
	public function createAction() {
		$this->getModel()->dispenseBackup();
		
		$response = new responseBup();
		$response->addMessage(langBup::_('Backup complete'));
		$response->ajaxExec();
	}
	
	/**
	 * Restore Action
	 * Restore system and/or database from backup
	 */
	public function restoreAction() {
		$request  = reqBup::get('post');
		$response = new responseBup();
		$filename = $request['filename'];
		$model    = $this->getModel();
		
		$result = $model->loadBackup($filename);
		
		if (false === $result) {
			$response->addError($model->getErrors());
		}
		else {
			$response->addData($result);
			$response->addMessage(langBup::_('Done!'));
		}
		
		$response->ajaxExec();
	}
	
	/**
	 * Download Action
	 */
	public function downloadAction() {
		$request  = reqBup::get('get');
		$model    = $this->getModel();
		$filename = $request['download'];
		
		if (is_admin()) {
			redirect(
					get_bloginfo('siteurl')
					. '/wp-content/' 
					. basename($model->getBackupsPath()) 
					. '/'
					. $filename);
		}
		else {
			exit(404);
		}
	}
	
	/**
	 * Remove Action
	 */
	public function removeAction() {
		$request  = reqBup::get('post');
		$response = new responseBup();
		$model    = $this->getModel();
		
		if ($model->remove($request['filename']) === true) {
			$response->addMessage(langBup::_('Backup successfully removed'));
		}
		else {
			$response->addError(langBup::_('Unable to delete backup'));
		}
		
		$response->ajaxExec();
	}
	
	/**
	 * Get model
	 * @param  string $name
	 * @return \backupModelBup
	 */
	public function getModel($name = '') {
		return parent::getModel($name);
	}
	
	/**
	 * 
	 * @param  string $template
	 * @param  array  $data
	 * @return string
	 */
	public function render($template, $data = array()) {
		return $this->getView()->getContent('backup.' . $template, $data);
	}
}