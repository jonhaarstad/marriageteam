jQuery(document).ready(function() {
	var j = jQuery.noConflict();
	
	// Toggle 'Send to'
	j('.bupSendTo').on('click', function() {
		j(this).parent().next().toggle();
	});
	
	// Download
	j('.bupDownload').on('click', function() {
		var filename = j(this).attr('data-filename');
		
		BackupModule.download(filename);
	});
	
	// Delete
	j('.bupDelete').on('click', function() {
		var filename = j(this).attr('data-filename'),
			id       = j(this).attr('data-id');
			
		if (confirm('Are you sure?')) {
			BackupModule.remove(id, filename, this);
		}
	});
	
	// Restore
	j('.bupRestore').on('click', function() {
		var filename = j(this).attr('data-filename'),
			id       = j(this).attr('data-id');
			
		BackupModule.restore(id, filename);
	});
	
	// Create
	j('#bupAdminMainForm').submit(function(event) {
		event.preventDefault();
		BackupModule.create(this);
	});
	
	jQuery('.bupSendToBtn').on('click', function(clickEvent) {
		clickEvent.preventDefault();
		var providerModule = jQuery(this).attr('data-provider'),
			providerAction = jQuery(this).attr('data-action'),
			files          = jQuery(this).attr('data-files'),
			id             = jQuery(this).attr('id');

		console.log('Module: ' + providerModule);
		console.log('Action: ' + providerAction);
		console.log('Files: '  + files);
		BackupModule.upload(providerModule, providerAction, files, id);

	});
});

/**
 * Backup Module
 */
var BackupModule = {
	download: function(filename) {
		document.location.href = document.location.href + '&download=' + filename;
	},
	remove: function(id, filename, button) {
		jQuery.sendFormBup({
			msgElID: 'MSG_EL_ID_' + id,
			data: {
				'reqType':  'ajax',
				'page':     'backup',
				'action':   'removeAction',
				'filename': filename
			},
			onSuccess: function(response) {
				if (response.error === false) {
					jQuery(button).parent().parent().remove();
				}
			}
		});
	},
	restore: function(id, filename) {
		jQuery.sendFormBup({
			msgElID: 'MSG_EL_ID_' + id,
			data: {
				'reqType':  'ajax',
				'page':     'backup',
				'action':   'restoreAction',
				'filename': filename
			},
			onSuccess: function(response) {
				console.log(response);
				if (response.error === false) {
					location.reload(true);
				}
			}
		});
	},
	create: function(form) {
		
		var progress = jQuery('.main-progress-bar');
		
		progress.show();
		
		jQuery(form).sendFormBup({
			msgElID: 'BUP_MESS_MAIN',
			onSuccess: function() {
				progress.hide();
			}
		});
	},
	upload: function(providerModule, providerAction, files, identifier) {
		jQuery.sendFormBup({
			msgElID: 'MSG_EL_ID_' + identifier,
			data: {
				page:    providerModule, // Module
				action:  providerAction, // Action
				reqType: 'ajax',         // Request type
				sendArr: files           // Files
			}
		});
	}
};