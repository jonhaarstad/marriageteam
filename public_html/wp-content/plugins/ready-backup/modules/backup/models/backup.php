<?php

/**
 * Backup Model
 * @package ReadyBackup\Modules\Backup
 * @since   2.0
 * @author  Artur Kovalevsky
 */
class backupModelBup extends modelBup {
	
	const BACKUP_DATABASE_EXT   = 'sql';
	const BACKUP_FILESYSTEM_EXT = 'zip';
	
	/**
	 * Module configurations
	 * @var array
	 */
	private $_config = array();
	
	/**
	 * Backup file list
	 * @var array
	 */
	private $_files = array();
	
	/**
	 * Template for file names
	 * @var string
	 */
	private $_filename = 'backup_{datetime}_id{fileid}.{extension}';
	
	/**
	 * Backup indentifier
	 * @var integer
	 */
	private $_identifier = null;
	
	/**
	 * List of queries that need to look for in the SQL file
	 * @var array
	 */
	private $_queries = array('CREATE', 'INSERT', 'DROP');

	/**
	 * Get module configurations
	 * 
	 * New options since version 2.0:
	 *  - Force Update
	 *		Disables any file security checks and restores the database 
	 *		or file system, ignoring any errors
	 *  - Safe Update
	 *		Uses transactions to restore the database (if possible)
	 *  - Replace Newer
	 *		If enabled, when you restore the file system will be 
	 *		replaced with all the files. 
	 *		If disabled, you will be restored from the archive only those files that 
	 *		are not on the server
	 *
	 * @return array
	 */
	public function getConfig() {
		if(empty($this->_config)) {
			$options = frameBup::_()->getModule('options');
			$this->_config = array(
				'full_backup'   => (bool)$options->get('full'),
				'plugins_dir'   => (bool)$options->get('plugins'),
				'themes_dir'    => (bool)$options->get('themes'),
				'uploads_dir'   => (bool)$options->get('uploads'),
				'database'      => (bool)$options->get('database'),
				'any_dir'       => (bool)$options->get('any_directories'),
				'exclude'       => $options->get('exclude'),
				'warehouse'     => realpath(ABSPATH . $options->get('warehouse')) . DIRECTORY_SEPARATOR,
				'dest'          => $options->get('glb_dest'),
				// Since 2.0
				'force_update'  => (bool)$options->get('force_update'),
				'safe_update'   => (bool)$options->get('safe_update'),
				'replace_newer' => (bool)$options->get('replace_newer'),
			);
		}
		
		return $this->_config;
	}
	
	public function dispenseBackup() {
		$config = $this->getConfig();
		
		if ($config['full_backup']) {
			$this->createFilesystemBackup();
			$this->createDatabaseBackup();
		}
		
		if ($config['database']) {
			$this->createDatabaseBackup();
		}
		
		foreach (array(
			'plugins_dir', 'themes_dir', 'uploads_dir', 'any_dir'
		) as $key) 
		{
			if ($config[$key] === true) {
				$this->createFilesystemBackup();
			}
		}
	
		$destination = $config['dest'];
		$handlers    = $this->getDestinationHandlers();
		
		if (array_key_exists($destination, $handlers)) {
			$handler = $handlers[$destination];
			$result  = call_user_func_array($handler, array($this->_files));
			if ($result === true || $result == 200 || $result == 201) {
				$this->removeFiles();
				return true;
			}
			
			return false;
		}
		
		return true;
	}
	
	public function getDestinationHandlers() {
		$handlers = array();
		$handlers = dispatcherBup::applyFilters('adminBackupUpload', $handlers);
		
		return $handlers;
	}
	
	public function removeFiles() {
		$config = $this->getConfig();
		
		foreach ($this->_files as $file) {
			unlink($config['warehouse'] . $file);
		}
	}
	
	public function loadBackup($filename) {
		$config = $this->getConfig();
		if (empty($filename)) {
			$this->pushError(langBup::_('Filename cant be empty'));
			return false;
		}
		
		$filename = $config['warehouse'] . $filename;
		
		switch(strtolower(pathinfo($filename, PATHINFO_EXTENSION))) {
			case self::BACKUP_DATABASE_EXT:
				return $this->restoreDatabase($filename);
				
			case self::BACKUP_FILESYSTEM_EXT:
				return $this->restoreFilesystem($filename);
			
			default:
				$this->pushError(langBup::_('Unable to recover from this backup'));
				return false;
		}
	}
	
	/**
	 * Return next ID
	 * @return integer
	 */
	protected function getId() {
		$config = $this->getConfig();
		
		if ($this->_identifier === null) {
			$files  = scandir($config['warehouse']);

			$matches = array();
			$results = array();

			foreach($files as $file) {
				if(preg_match('/id([\d]+)/', $file, $matches)) {
					$results[] = $matches[1];
				}
			}
		
			if(!empty($results)) {
				$this->_identifier = max($results) + 1;
			}
			else {
				$this->_identifier = 1;
			}
		}
		
		return $this->_identifier;
	}
	
	/**
	 * Write to file
	 * @param string $data
	 * @param string $type
	 * @return boolean
	 */
	protected function write($data, $type) {
		$config = $this->getConfig();
		$path = $config['warehouse'];
		$file = $this->createName($type);

		if(file_put_contents($path . $file, $data) === false) {
			$this->logMessage('Writing to a file failed: ' . $path . $file);
			return false;
		}
		
		$this->logMessage('Writing to a file is completed: ' . $path . $file);
		$this->_files[] = $file;
		return $path . $file;
	}
	
	/**
	 * Read from file
	 * @param  string $filename
	 * @return null|string
	 */
	protected function read($filename) {
		if($this->backupExists($filename) === false) {
			return null;
		}
		
		return file_get_contents($filename);
	}

	protected function createName($ext) {
		$date = date('Y_m_d-H_i_s'); 
		$id   = $this->getId();
		
		return str_replace(
			array('{datetime}', '{fileid}', '{extension}'),
			array($date, $id, $ext),
			$this->_filename
		);
	}
	
/* --------------------------------------------------------------------------- *
 * This part of the file is responsible for working with the filesystem
 * All methods presented here ANYWAY USED
 * -------------------------------------------------------------------------- */
	
	/**
	 * Create and archive to ZIP filesystem backup
	 * @return boolean
	 */
	protected function createFilesystemBackup() {
		$this->logMessage(str_repeat('=', 100), false);
		$this->logMessage('Begin to create a backup copy of the file system');
		$this->logMessage(str_repeat('=', 100), false);
		
		// --- Configure backup folders
		$backupConfig = $this->getDirectoryParams();
		$dir     = $backupConfig['directory'];
		$exclude = $backupConfig['exclude'];
		
		// --- Options & Initializations
		$ZIP   = new Zip();									// Instance of ZIP Archiver
		$files = $this->getFilesFrom($dir, $exclude);		// An array of files
		$limit = intval(ini_get('memory_limit')) * 1048576;	// Memory Limit
		
		// --- Setup backup name
		$config = $this->getConfig();
		$name   = $this->createName(self::BACKUP_FILESYSTEM_EXT);
		$path   = $config['warehouse'];
		
		$this->logMessage('Start writing to a file: ' . $path . $name);
		$ZIP->setZipFile($path . $name);
		
		foreach ($files as $file) {
			$this->logMessage('Handle file: ' . basename($file) . ' (~'. $this->humanFilesize(filesize($file)) .')');
			$path = str_replace(realpath($dir), '', $file);
			
			if (filesize($file) > $limit) {
				$this->logMessage('File size is larger than the limit server memory. Start a partial write.');
				$stream = @fopen($file, 'rb');	
				if ($stream) {
					$ZIP->addLargeFile($stream, $path);
				}
			}
			else {
				$data = file_get_contents($file);
				$ZIP->addFile($data, $path);
			}
		}
		$archiveSize = $ZIP->getArchiveSize();
		$ZIP->finalize();
		unset($ZIP); // trigger destructor (bug in lib)
		$this->logMessage('ZIP archive created (~'. $this->humanFilesize($archiveSize) .'), ' . count($files) . ' files is written');
	
		$this->_files[] = $name;
		
		return true;
	}
	
	/**
	 * Restore filesystem from ZIP file
	 * @param  string $filename
	 * @return boolean
	 */
	protected function restoreFilesystem($filename) {
		$config = $this->getConfig();
		$args   = array();
		
		if ($config['replace_newer'] === true) {
			array_push($args, PCLZIP_OPT_PATH, ABSPATH, PCLZIP_OPT_REPLACE_NEWER);
		}
		else {
			array_push($args, PCLZIP_OPT_PATH, ABSPATH);
		}
		
		if (!file_exists($filename)) {
			$this->pushError(langBup::_('ZIP file does not exists'));
			return false;
		}
		
		$pcl = new PclZip($filename);
		$result = call_user_func_array(array($pcl, 'extract'), $args);

		if ($result === 0 OR $result === false) {
			$this->pushError(langBup::_('An error occurred while unpacking the archive'));
			return false;
		}

		return true;
	}
	
	/**
	 * Configure the module based on the configuration
	 * @return associative array with <directory> and <exclude> indexes
	 */
	protected function getDirectoryParams() {
		$config  = $this->getConfig();
		$exclude = array();
		
		// Cut spaces from excluded directories
		if (!empty($config['exclude'])) {
			$exclude = array_map('trim', explode(',', $config['exclude']));
		}
		
		array_push($exclude, 'upready');
		
		if ($config['any_dir'] === false) {
			$wpcontentFiles = scandir(ABSPATH . DIRECTORY_SEPARATOR . 'wp-content');
			foreach ($wpcontentFiles as $node) {
				if (is_dir($node) && $node != '.' && $node != '..') {
					if (!in_array($node, array('plugins', 'themes', 'uploads', 'upready'))) {
						array_push($exclude, $node);
					}
				}
			}
		}
		
		foreach (array(
			'themes'  => $config['themes_dir'],
			'plugins' => $config['plugins_dir'],
			'uploads' => $config['uploads_dir'],
		) as $folder => $value) {
			if ($value === false) {
				array_push($exclude, $folder);
			}
		}
		
		return array('directory' => ABSPATH, 'exclude' => $exclude);
	}
	
	/**
	 * Convert bytes to human-friendly values
	 * @param integer $bytes
	 * @param integer $decimals
	 * @return string
	 */
	protected function humanFilesize($bytes, $decimals = 2) {
		$sz = 'BKMGTP';
		$factor = floor((strlen($bytes) - 1) / 3);
		
		return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$sz[$factor];		
	}
	
	/**
	 * Get total files, processed files and percent of processed files
	 * @return array
	 */
	public function getFilesCounter() {
		return array(
			'files' => $this->_files,
			'proc'  => $this->_processed,
			'perc'  => number_format(($this->_processed / $this->_files) * 100, 0),
		);
	}
	
	/**
	 * Recursively walking given directory and returns all the files in it
	 * @param  string $directory Relative or absolute path to the directory
	 * @param  array  $exclude   An array of directories that should not be included in the list (i.e array('themes', 'plugins'))
	 * @return array
	 */
	protected function getFilesFrom($directory, $exclude = array()) {
		$nodes = array();
	
		foreach (glob(realpath($directory) . '/*') as $node) {
			if (is_dir($node)) {
				// Exclude folders
				if (!empty($exclude)) {
					$needle = end(explode(DIRECTORY_SEPARATOR, $node));
					if (!in_array($needle, $exclude)) {
						$nodes = array_merge($nodes, $this->getFilesFrom($node, $exclude));
					}
				}
				else {
					$nodes = array_merge($nodes, $this->getFilesFrom($node));
				}
			}
			else {
				array_push($nodes, $node);
			}
		}
	
		return $nodes;		
	}
	
/* --------------------------------------------------------------------------- *
 * This part of the file is responsible for working with the database
 * All methods presented here ANYWAY USED
 * -------------------------------------------------------------------------- */
	
	/**
	 * Create and return database backup
	 * @global \wpdb  $wpdb
	 * @global string $wp_db_version
	 * @global string $wp_version
	 * @return string
	 */
	protected function createDatabaseBackup() {
		global $wpdb;
		global $wp_db_version;
		global $wp_version;
		
		$database = array(
			sprintf('-- Created with %s %s', BUP_S_WP_PLUGIN_NAME, BUP_VERSION),
			'-- http://readyshoppingcart.com/product/wordpress-backup-and-restoration-plugin/' . PHP_EOL,
			sprintf('-- Do not change these values if you doesnt want broke the database during recovery:'),
			sprintf('-- @dbrev=%s;', $wp_db_version),        // database revision
			sprintf('-- @wpcrv=%s;', $wp_version),           // wordpress verison
			sprintf('-- @plgnv=%s;', BUP_VERSION) . PHP_EOL, // plugin version
		);
		
		$this->logMessage(str_repeat('=', 100), false);
		$this->logMessage('Create a database dump');
		$this->logMessage(str_repeat('=', 100), false);
		
		// Create database backup
		$tables = $wpdb->get_results('SHOW TABLES', ARRAY_N);
		foreach($tables as $table) {
			$this->logMessage('Handle table: ' . $table[0]);
			$table['name'] = $table[0];
			
			// Drop table query
			$table['drop'] = 'DROP TABLE IF EXISTS `'.$table['name'].'`#endQuery' . PHP_EOL;
			
			// Create table query
			$createQuery = $wpdb->get_row('SHOW CREATE TABLE `'.$table['name'].'`', ARRAY_A);
			if(isset($createQuery['Create Table'])) {
				$table['create'] = $createQuery['Create Table'] . '#endQuery' . PHP_EOL;
			}
			
			// Get table columns
			$tableDataQuery = 'SELECT * FROM `'.$table['name'].'`';
			$tableColsQuery = 'SHOW COLUMNS FROM `'.$table['name'].'`';
			
			// Parse columns
			$tableCols = $this->parseColumns($wpdb->get_results($tableColsQuery, ARRAY_A));
			$tableData = $this->parseData($wpdb->get_results($tableDataQuery, ARRAY_N));
			foreach($tableData as $data) {
				$table['insert'] .= 'INSERT INTO `' . $table['name'] . '` (' . implode(', ', $tableCols) . ') VALUES (' . $data . ')' . '#endQuery' . PHP_EOL;
			}
			// Push results to stack
			$database[] = implode(PHP_EOL, array($table['drop'], $table['create'], $table['insert']));
		}
		
		// Save database backup
		$this->write(implode(PHP_EOL, $database), self::BACKUP_DATABASE_EXT);
		return true;
	}
	
	/**
	 * Restore database from SQL file
	 * @global \wpdb $wpdb
	 * @param  string $filename full path to backup file
	 * @return boolean
	 */
	public function restoreDatabase($filename) {
		global $wpdb;
		
		if($this->backupExists($filename) === false) {
			$this->pushError(sprintf('Backup %s doesn\t exists', basename($filename)));
			return false;
		}
		
		// Read SQL file
		$input = $this->read($filename);
		
		// Validate metdata
		$metadata = $this->parseMetadata($input);
		if($this->validateMetadata($metadata) === false) {
			return false;
		}

		// Is it old backup?
		if (preg_match('/(database|full)/ui', $filename)) {
			return $this->restoreDatabaseFallback($filename);
		}
		
		// Parse queries
		$queries = $this->parseQueries($input);
		if($queries === null) {
			$this->pushError('Unable to parse queries from SQL file');
			return false;
		}
		
		// Run parsed quesries
		if($this->runQueries($queries) === false) {
			$this->pushError(sprintf('An error occurred while restoring the '
					. 'database: %s', $wpdb->last_error));
			return false;
		}
		
		return true;
	}

	/**
	 * Restore database from the old (v0.3.2 and lower) sql dump
	 * @deprecated since version 0.3.2
	 * @global \wpdb $wpdb
	 * @param  string $source
	 * @return boolean
	 */
	protected function restoreDatabaseFallback($source) {
		global $wpdb;
		$ret = true;
			if ($handle = @fopen($source, "r")) {
				if ($this->_clearDatabaseFallback($source)) {
					while (($buffer = fgets($handle)) !== false) {
						$wpdb->query($buffer);
					}
					
					if (!feof($handle)) {
						$this->pushError(langBup::_("Error: unexpected fgets() fail\n"));
					}
					
					fclose($handle);
				} else {
					$this->pushError(langBup::_('Unable to clear the database before restoring'));
					$ret = false;
				}
			} else {
						$this->pushError(langBup::_(sprinf('File not found: %s', $source)));
						$ret = false;
			}

		return $ret;
	}	
	
	/**
	 * Clear database fallback
	 * @deprecated since version 0.3.2
	 * @global \wpdb $wpdb
	 * @param type $source
	 * @return boolean
	 */
	protected function _clearDatabaseFallback($source) {
		global $wpdb;
		$ret = true;
		if ($handle = @fopen($source, "r")) {
			$i = 0;
			while (($buffer = fgets($handle)) !== false) {
				$i++;
				if ($i <= 4) continue;
				
				preg_match_all('~CREATE TABLE `(.*)` \( ~', $buffer, $out);
				if ($out[1][0]) {
					$wpdb->query("DROP TABLE `{$out[1][0]}`;");
				} else 
					break;
			}
		} else {
			$this->pushError(langBup::_('Can not find file'.$source));
			$ret = false;
		}
		return $ret;		
	}
	
	/**
	 * Validate metadata of SQL file and stop restore if its invalid
	 * @param  array $metdata
	 * @return boolean
	 */
	protected function validateMetadata($metdata) {
		global $wp_db_version;
		global $wp_version;
		
		$config = $this->getConfig();
		// If force update enabled then we doesnt have any reasons to validate metadata
		if($config['force_update'] === true) {
			return true;
		}
		
		if($metdata['dbrev'] != $wp_db_version) {
			$this->pushError(
				langBup::_('Revision of backup and your database do not match. '
						. 'You must enable the Force Update options to update '
						. '(at one\'s own risk)')
			);
			return false;
		}
		
		if($metdata['wpcrv'] != $wp_version) {
			$this->pushError(
				langBup::_('This backup was made on another version of WordPress. '
						. 'You must enable the Force Update options to update '
						. '(at one\'s own risk)')
			);
			return false;
		}
		
		if($metdata['plgnv'] != BUP_VERSION) {
			$this->pushError(
				langBup::_('Backup was created with a different version of the plugin. '
						. 'You must enable the Force Update options to update '
						. '(at one\'s own risk)')
			);
			return false;
		}
		
		return true;
	}

	/**
	 * Run parsed queries from SQL file
	 * @global \wpdb   $wpdb
	 * @param  array   $queries
	 * @return boolean
	 */
	protected function runQueries($queries = array()) {
		global $wpdb;
		
		$config = $this->getConfig();
		$transaction = $this->getTransactionSupport();
		
		if(empty($queries) OR $queries === null) {
			return false;
		}
		
		// Start transaction if safe update enabled or set error if transaction is unsupported
		if($config['safe_update'] === true) {
			if($transaction === true) {
				@mysql_query('SET AUTOCOMMIT=0', $wpdb->dbh);
				@mysql_query('START TRANSACTION', $wpdb->dbh);
			}
			else {
				$this->pushError(langBup::_('Your MySQL server does not support transactions, "safe update" unavailable'));
				return false;
			}
		}
		
		foreach($queries as $query) {
			if($wpdb->query($query) === false) {
				// Rollback transaction if safe update enabled
				if($config['safe_update'] === true && $transaction === true) {
					@mysql_query('ROLLBACK', $wpdb->dbh);
				}
				return false;
			}
			else {
				// dblog - true
			}
		}
		
		// Commit transaction if safe update enabled
		if($config['safe_update'] === true && $transaction === true) {
			@mysql_query('COMMIT', $wpdb->dbh);
		}
		return true;
	}
	
	/**
	 * Check for MySQL server verion, db handler access and other
	 * @global \wpdb $wpdb
	 * @return boolean
	 */
	public function getTransactionSupport() {
		global $wpdb;
		
		if(!function_exists('mysql_query') && function_exists('mysql_get_server_info')) {
			return false;
		}
		
		// Can we get access to the database handler?
		if(is_resource($wpdb->dbh) === false) {
			return false;
		}
		
		// Is it 5.x.x version?
		$serverInfo = mysql_get_server_info($wpdb->dbh);
		if(substr($serverInfo, 0, 1) != 5) {
			return false;
		}
		
		return true;
	}
	
	/**
	 * Parse matrix of columns to the one dimensional array
	 * @param  array $columns
	 * @return array
	 */
	protected function parseColumns($columns) {
		$result = array();
		
		foreach($columns as $column) {
			$result[] = $column['Field'];
		}
		
		return $result;
	}
	
	/**
	 * Parse columns data from matrix to the one dimensional array
	 * @param  array $data
	 * @return array
	 */
	protected function parseData($data) {
		$result = array();
		
		foreach($data as $column) {
			$values = array_map(array($this, '_addQuotes'), array_values($column));

			$result[] = implode(', ', $values);
		}
		
		return $result;
	}
	
	/**
	 * Parse queries from sql file
	 * @param  string $input
	 * @return null|array
	 */
	protected function parseQueries($input) {
		$queries = array();
		$permitted = strtoupper(implode('|', $this->_queries));
		
		// We parse only permitted queries from file
		preg_match_all('/(('.$permitted.')(.*?))#endQuery/su', $input, $queries);
		
		if(isset($queries[1]) && !empty($queries[1])) {
			return $queries[1];
		}
		
		return null;
	}
	
	/**
	 * Parse metdata from SQL file
	 * @param  string $input
	 * @return null|array
	 */
	protected function parseMetadata($input) {
		$metadata = array();
		preg_match_all('/--\s*@(.*);/', $input, $metadata);
		
		if(isset($metadata[1]) && !empty($metadata[1])) {
            $variables = array();
			// Create an array with key => val from strings with key=val
			foreach($metadata[1] as $variableType) {
				$e = explode('=', $variableType);
				$variables[trim($e[0])] = trim($e[1]);	
			}
			return $variables;
		}
		
		return null;
	}
	
	/**
	 * Is backup exists on server
	 * @param  string $filename
	 * @return boolean
	 */
	public function backupExists($filename) {
		return file_exists($filename);
	}
	
	/**
	 * Add quotes to string values
	 * @param  string $value
	 * @return string|integer
	 */
	public function _addQuotes($value) {
		global $wpdb;
	
		if(is_numeric($value)) {
			return $value;
		}
		$value = "'". $wpdb->_real_escape($value) . "'";
		
		return $value;
	}
	
/* --------------------------------------------------------------------------- *
 * Common methods
 * -------------------------------------------------------------------------- */
	
	public function getOldBackupsList() {
		$config  = $this->getConfig();
		$pattern = '/(backup_([0-9_-]*)-(database|full)_id([\d]+))\.(zip|sql)/ui';
		$backups = array();
		$matches = array();
		
		foreach (scandir(realpath($config['warehouse'])) as $node) {
			if (preg_match($pattern, $node, $matches)) {
				list($name, $rawname, $datetime, $type, $id, $ext) = $matches;
				
				$e = explode('-', $datetime);
				$date = str_replace('_', '-', $e[0]);
				$time = str_replace('_', ':', $e[1]);
				
				$backups[] = array(
					'id' => $id,
					'name' => $name,
					'raw'  => $rawname,
					'ext'  => $ext,
					'type' => $type,
					'date' => $date,
					'time' => $time,
				);
			}
		}
		
		return $backups;
	}
	
	/**
	 * Return all finded backups
	 * @return null|array
	 */
	public function getBackupsList() {
		$config  = $this->getConfig();
		$pattern = '/(backup_([0-9_-]*)_id([0-9]+))\.(zip|sql)/ui';
		$backups = array();		
		
		$dir = scandir(realpath($config['warehouse']));
		
		foreach ($dir as $file) {
			$matches = array();
	
			if (preg_match($pattern, $file, $matches)) {
				list ($name, $rawname, $date, $id, $extension) = $matches;
				
				$e = explode('-', $date);
				$datetime['date'] = str_replace('_', '-', $e[0]);
				$datetime['time'] = str_replace('_', ':', $e[1]);
			
				// Generate backups array by ID
				$backups[$id][strtolower($extension)] = array(
					'id'   => $id,
					'name' => $name,
					'raw'  => $rawname,
					'ext'  => $extension,
					'date' => $datetime['date'],
					'time' => $datetime['time'],
				);
			}
		}
		krsort($backups);
		return $backups;
	}
	
	/**
	 * Get path to backups
	 * @return string
	 */
	public function getBackupsPath() {
		$config = $this->getConfig();
		return $config['warehouse'];
	}
	
	/**
	 * Delete backup
	 * @param  string $filename
	 * @return boolean
	 */
	public function remove($filename) {
		$path = $this->getBackupsPath();
		return unlink($path . $filename);
	}
	
	public function logMessage($text, $insertDate = true) {
		$id     = $this->getId();
		$config = $this->getConfig();
		$date   = date('Y-m-d H:i:s');
		
		$filename = $config['warehouse'] . 'log-id' . $id . '.txt';
		$filedata = null;
		
		if ($insertDate === true) {
			$filedata = "[{$date}] :: ";
		}
		
		$filedata.= "{$text}" . PHP_EOL;
		
		file_put_contents($filename, $filedata, FILE_APPEND);
	}
}