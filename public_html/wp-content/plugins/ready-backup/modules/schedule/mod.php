<?php
class scheduleBup extends moduleBup {
	public function init() {
		add_action('bup_cron_hour', array($this, 'backupSchedule')); 
		add_action('bup_cron_day', array($this, 'backupSchedule')); 
		add_action('bup_cron_day_twice', array($this, 'backupSchedule')); 
		add_action('bup_cron_weekly', array($this, 'backupSchedule')); 
		add_action('bup_cron_monthly', array($this, 'backupSchedule')); 
		
		add_filter('cron_schedules', array($this, 'bup_cron_add_interval'));  
		
		//dispatcherBup::addFilter('cron_schedules', array($this, 'bup_cron_add_interval'));
		parent::init();
	}
	
	
	function bup_cron_add_interval( $schedules ) {  
    
	  $schedules['weekly'] = array(  
		  'interval' => 604800,  //
		  'display' => __( 'Once Weekly' )  
	  );  
	  
	  $schedules['monthly'] = array(
		'interval' => 2635200,
		'display' => __('Once a month')
	  );
	  
  // -- test
	 /*  $schedules['test_hour'] = array(  
		  'interval' => 240,  //
		  'display' => __( 'Test' )  
	  ); */ 
	  
	  /*
	  $schedules['test_2daily'] = array(  
		  'interval' => 5,  //
		  'display' => __( 'Test' )  
	  );  
	  $schedules['test_daily'] = array(  
		  'interval' => 5,  //
		  'display' => __( 'Test' )  
	  );  
	  $schedules['test_weekly'] = array(  
		  'interval' => 5,  //
		  'display' => __( 'Test' )  
	  );  
	  $schedules['test_monthly'] = array(  
		  'interval' => 5,  //
		  'display' => __( 'Test' )  
	  ); */ 
	  
      return $schedules;  
	  
	}
	// -- test
	function test_schedule() { 
	$my_post = array('post_title' => 'Новая запись создана:' . date('Y-m-d в H:i:s'), 
                     'post_content' => 'Это очередная запись <br> Unix-время создания:' . time(), 
					 'post_status' => 'publish', 
                     'post_author' => 1, 
                     'post_category' => array(0)
                      ); 
		wp_insert_post( $my_post ); 
	}
	
	public function backupSchedule(){
		frameBup::_()->getModule('backup')->getModel()->simplezip('schedule');
	}
	
}