<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>MarriageTeam Home</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="assets/css/bootstrap.css" rel="stylesheet">
</head>
<body>

<div id="top">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 top-nav">
				<nav>
					<ul>
						<li><a href="#">Site Map</a></li>
						<li><a href="/blog">Blog</a></li>
						<li><a href="/coaches">Coaches</a></li>
						<li><a href="/give">Give</a></li>
					</ul>
				</nav>
			</div>
		</div>
	</div>
</div>

<div id="top-back">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-6">
				<div class="logo"><img src="assets/images/logo.png"></div>
				<ul class="home-nav-block">
					<li>
						<a href="#"><span><strong>Find</strong> A Marriage Coach</span><br>
							Coaches work with couples to help them achieve their goals. Premarital, Enrichment, Restoration
</a>
					</li>
					<li>
						<a href="#"><span><strong>Become</strong> a Marriage Coach</span><br>
							You can become a MarriageTeam coach couple or start a program in your church.</a>
					</li>
				</ul>
				<p>Slideshow</p>
			</div>
			<div class="col-xs-12 col-sm-6 home-top-right">
				<ul>
					<li><a href="#">Pastors</a></li>
					<li><a href="#">Events</a></li>
				</ul>
			</div>
		</div>
	</div>
</div>

<div id="gold-bar">
	<div class="container">
		<div class="row">
			<div class="col-sm-9">
				<blockquote>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor conquer incididunt match ut bologne labore more et dolore something magna aliqua. <span>~ Name Goes Here</span></blockquote>
			</div>
			<div class="col-sm-3">
				<img src="" alt="Image Goes Here">
			</div>
		</div>
	</div>
</div>

<div id="white-back-content">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<h1><span>Find</span> A Marriage Coach</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6">
				<h2>Can marriage coaching really save my marriage?</h2>
				<p>Marriage coaching is highly effective for couples who are willing to work together to achieve their common goals.  Coaches help couples resolve issues by facilitating a process of discovery that leads to creating mutually satisfying   agreements.  Over 85% of the couples contemplating divorce report that they are no longer considering divorce after completing coaching.</p>
				<h2>Why should I try marriage coaching rather than counseling?</h2>
				<p>Many couples report that coaching was more effective for them because each session results in new tools and specific agreements on what to do differently to get different results.  Coaches help couples create mutual accountability for the changes they want.</p>
			</div>
			<div class="col-sm-6">
				<h2>How do I know if I need coaching?</h2>
				<p>Olympic athletes have coaches because they want to achieve their goals. Take a short inventory and see if coaching can help improve your marriage.</p>
				<h2>How do I get a coach?</h2>
				<p>You can get a coach by completing an on-line form or calling 866 831-4201.</p>
				<p><a href="#" class="btn btn-primary"><span>Find</span> a Coach</a><br>
				<div class="red-small">Sign-up for our FREE monthly newsletter</div></p>
			</div>
		</div>
	</div>
</div>

<div id="blue-back-content">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<h1><span>Become</span> A Marriage Coach</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6">
				<h2>How Do We Become a Coach Couple?</h2>
				<p>You can become a coach couple by completing 24 hours of coach training.  You may want to sponsor a training program at your church.  Learn more.</p>
			</div>
			<div class="col-sm-6">
				<h2>How Can I become More Involved with Strengthening Marriages?</h2>
				<p>There are several volunteer opportunities to support and promote marriage coaching.</p>
				<p><a href="#" class="btn btn-primary"><span>Become</span> a Coach</a><br>
				<div class="blue-small">Sign-up for our FREE monthly newsletter</div></p>
			</div>
		</div>
	</div>
</div>

<div id="footer">
	<div class="container">
		<div class="row">
			<div class="col-sm-9">
				<ul>
					<li>
						<a href="#">About</a>
						<ul>
							<li><a href="#">Page Title</a></li>
							<li><a href="#">Page Title</a></li>
							<li><a href="#">Page Title</a></li>
							<li><a href="#">Page Title</a></li>
						</ul>
					</li>
					<li>
						<a href="#">Couples</a>
						<ul>
							<li><a href="#">Page Title</a></li>
							<li><a href="#">Page Title</a></li>
							<li><a href="#">Page Title</a></li>
							<li><a href="#">Page Title</a></li>
						</ul>
					</li>
					<li>
						<a href="#">Coaches</a>
						<ul>
							<li><a href="#">Page Title</a></li>
							<li><a href="#">Page Title</a></li>
							<li><a href="#">Page Title</a></li>
							<li><a href="#">Page Title</a></li>
						</ul>
					</li>
					<li>
						<a href="#">Counselors <br>&amp; Clergy</a>
						<ul>
							<li><a href="#">Page Title</a></li>
							<li><a href="#">Page Title</a></li>
							<li><a href="#">Page Title</a></li>
							<li><a href="#">Page Title</a></li>
						</ul>
					</li>
					<li>
						<a href="#">Resources</a>
						<ul>
							<li><a href="#">Page Title</a></li>
							<li><a href="#">Page Title</a></li>
							<li><a href="#">Page Title</a></li>
							<li><a href="#">Page Title</a></li>
						</ul>
					</li>
					<li>
						<a href="#">Events</a>
						<ul>
							<li><a href="#">Page Title</a></li>
							<li><a href="#">Page Title</a></li>
							<li><a href="#">Page Title</a></li>
							<li><a href="#">Page Title</a></li>
						</ul>
					</li>
				</ul>
			</div>
			<div class="col-sm-3">
				[SIGN-UP HERE]
			</div>
		</div>
	</div>
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
<script>
	// This is where the scripts go.
</script>

</body>
</html>