<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'mrgeadm_mtwdpdb');

/** MySQL database username */
define('DB_USER', 'mrgeadm_spruser');

/** MySQL database password */
define('DB_PASSWORD', 'J3c7!Ho*O/7');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '/7+v%=sz}G=~HCB[e1NsG^Ue*X?MdF`8I@ng|t(aq+-8jp-(.XgP8_dN-[U^dcLp');
define('SECURE_AUTH_KEY',  '|c{b?ixfmGip]!|hDd>WC@]173Ha:3?OVGC:-+hpFYe=F9-H!.cGqv97 CBnH:+h');
define('LOGGED_IN_KEY',    'iKfbopf+xRt+c`BZC8I<FUVpF%m1jJ29_^VC<i-?VU[4*cbpnBKR;-u4aB1Yy:6H');
define('NONCE_KEY',        ',*e6FUrln41Wv`tI|fI,?3rACI$k$Zu[VAHJZo[+hvpq(}I?@Anjl[&89v)8!l)M');
define('AUTH_SALT',        '51o|z-$TliHM%~^KcKe-G`pBemL+Q;Q,sv 5{{XE-3o0}:2|zGzqr<ZG&GFz9]*b');
define('SECURE_AUTH_SALT', 'JI1HVW|A6,{~+ZC9]&V19n7r-6V3s7hoWadb&-Lk+5q-A{aoxgwzp}6sd_9qrN4r');
define('LOGGED_IN_SALT',   'sNu#+ELI1R#2oq@8s%:$r]MJmjPizuRobkaZ.C_rA@rCKSAU.:A[WYIQqhb}yt=S');
define('NONCE_SALT',       ';|lg.|ya*wvQ-JOj ?riF}l?sy`JvYXs,fPV%;sm&VGkM+en^8kwfXWCyQ[qo-?)');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
